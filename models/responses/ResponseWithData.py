class ResponseWithData :
    def __init__(self,code,msg,results):
        self.code = code
        self.msg = msg
        self.results = results if results else []

class ResponsePaginateWithData :
    def __init__(self,code,msg,results,totals):
        self.code = code
        self.msg = msg
        self.totals = totals
        self.results = results if results else []