import json
import random

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE
from models.responses.ResponseError import ResponseError


class GenTemplate:
    template = ""
    data = ""
    def __init__(self):
        self.template = ""
        self.data = ""

    def generate(self, cmd, data, skillType):
        connector = DBConnector()
        try:
            print("")
        except Exception as e:
            print('StudentsService listMajor() Exception[1] ---> ', e)
            print('StudentsService listMajor() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

