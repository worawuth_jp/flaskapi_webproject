from flask_marshmallow import Schema
from marshmallow.fields import Str


class TeacherSchema(Schema) :
    class Meta:
        fields = ["teacher_id","teacher_firstname","teacher_lastname","teacher_phone","teacher_email"] #teacher_id	teacher_firstname	teacher_lastname	teacher_phone	teacher_email
        teacher_id = Str()
        teacher_firstname = Str()
        teacher_lastname = Str()
        teacher_phone = Str()
        teacher_email = Str()