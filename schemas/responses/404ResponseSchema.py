from flask_marshmallow import Schema
from marshmallow.fields import Str,Int

class WelcomeSchema(Schema) :
    class Meta:
        # Fields to expose
        fields = ["code","msg"]

    code = Int()
    msg = Str()