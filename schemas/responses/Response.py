
from flask_marshmallow import Schema
from marshmallow.fields import Str,Int,Nested
from constants.HTTP import HTTP_SUCCESS_MESSAGE, HTTP_SUCCESS_CODE

class BaseResponse(Schema) :
    class Meta:
        fields = ["code","msg"]
        code = Int(default=HTTP_SUCCESS_CODE)
        msg = Str(default=HTTP_SUCCESS_MESSAGE)

class ResponseWithData(Schema) :
    class Meta:
        # Fields to expose
        fields = ["data","results"]
    data = Nested(BaseResponse)

class Response(Schema) :
    class Meta:
        # Fields to expose
        fields = ["code","msg","stack"]
    code=Int()
    msg=Str()
    stack=Str()

class ResponseWithPaginate(Schema) :
    class Meta:
        fields = ["code","msg","results","totals"]

    code = Int()
    msg = Str()
    results = Nested(BaseResponse)
    totals = Int()