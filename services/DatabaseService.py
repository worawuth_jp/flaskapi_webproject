import json

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE
from constants.template import DATABASE, TABLES
from models.responses.ResponseError import ResponseError
from utils.ResponseUtil import ResponseUtil


class DatabaseService:
    def getAllDatabase(self):
        connector = DBConnector()
        try:
            results = []
            for row in DATABASE:
                results.append(row)
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('DatabaseService getAllDatabase() Exception[1] ---> ', e)
            print('DatabaseService getAllDatabase() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def getAllTable(self,db):
        connector = DBConnector()
        try:
            results = []
            for row in TABLES:
                results.append(row)
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('DatabaseService getAllDatabase() Exception[1] ---> ', e)
            print('DatabaseService getAllDatabase() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def queryDatabase(self,sql):
        connector = DBConnector()
        try:
            cursor = connector.getConnection()
            resQuery=cursor.execute(sql,multi=True)
            results = {}
            results['message'] = "query success"
            results['data'] = []
            for res in resQuery :
                if res.with_rows :
                    print(res)
                    for row in res :
                        results['data'].append(row)
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            # print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('DatabaseService queryDatabase() Exception[1] ---> ', e)
            print('DatabaseService queryDatabase() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()
