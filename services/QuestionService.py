import datetime
import json
import random

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE
from constants.question import ERROR_QUESTION_UNIQUE_MSG
from constants.template import TABLES
from constants.users import ERROR_USERNAME_UNIQUE_MSG
from models.responses.ResponseError import ResponseError
from services.TemplateService import TemplateService
from utils.ResponseUtil import ResponseUtil

class QuestionService:
    def getAllQuestions(self,subject_id,user_id):
        connector = DBConnector()
        try:

            sql = "SELECT * FROM questions WHERE 1=1"
            queryParams = ()
            if subject_id != "" :
                sql += " AND subject_id=%s"
                queryParams += (subject_id,)
            if user_id != "":
                sql = "SELECT * FROM questions INNER JOIN students ON students.subject_id = questions.subject_id WHERE 1=1"
                sql += " AND students.user_id=%s"
                queryParams = (user_id,)

            print(sql,user_id)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            print("Query Success ----->")
            results = []
            for row in cursor.fetchall():
                data = {}
                data['question_id'] = row['question_id']
                data['question_name'] = row['question_name']
                data['question_title'] = row['question_title']
                data['question_database'] = row['question_database']
                data['question_num'] = row['question_num']
                data['question_type'] = row['question_type']
                data['question_end_date'] = str(row['question_end_date'])
                data['question_start_time'] = str(row['question_start_time'])
                data['question_end_time'] = str(row['question_end_time'])
                data['question_status'] = row['question_status']
                data['question_answer'] = row['question_answer']
                data['question_point'] = float(row['question_point'])
                data['question_standard'] = row['question_standard']
                data['subject_id'] = row['subject_id']

                results.append(data)
            print("Get Result Success ----->",results)
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('QuestionService getAllQuestions() Exception[1] ---> ', e)
            print('QuestionService getAllQuestions() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def searchQuestion(self,text):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM questions WHERE 1=1"
            queryParams = ()
            if text != "" :
                sql += " AND (question_name LIKE %s OR question_database LIKE %s)"
                queryParams += ('%'+text+'%','%'+text+'%')
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            print("Query Success ----->")
            results = []
            for row in cursor.fetchall():
                data = {}
                data['question_id'] = row['question_id']
                data['question_name'] = row['question_name']
                data['question_title'] = row['question_title']
                data['question_database'] = row['question_database']
                data['question_num'] = row['question_num']
                data['question_type'] = row['question_type']
                data['question_end_date'] = str(row['question_end_date'])
                data['question_start_time'] = str(row['question_start_time'])
                data['question_end_time'] = str(row['question_end_time'])
                data['question_status'] = row['question_status']
                data['question_answer'] = row['question_answer']
                data['question_point'] = float(row['question_point'])
                data['question_standard'] = row['question_standard']
                data['subject_id'] = row['subject_id']

                results.append(data)
            print("Get Result Success ----->",results)
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('QuestionService searchQuestions() Exception[1] ---> ', e)
            print('QuestionService searchQuestions() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def addQuestion(self,name, title, database, num, type, endDate, startTime, endTime,answer,point,standard,subject_id,skillType="",sqlType="",status=1):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            temp = {}
            results = {}
            queryParams = []
            sql = "INSERT INTO `questions`(" \
                  "`question_title`, " \
                  "`question_name`, " \
                  "`question_database`, " \
                  "`question_num`, " \
                  "`question_type`, " \
                  "`question_end_date`, " \
                  "`question_status`," \
                  "`question_start_time`, " \
                  "`question_end_time`, " \
                  "`question_answer`, " \
                  "`question_point`, " \
                  "`question_standard`, " \
                  "`subject_id`) " \
                  "VALUES "
            if type == "A":

                for i in range(num):
                    if i > 0:
                        sql += ","
                    table = TABLES[random.randint(0, len(TABLES) - 1)]
                    temp = TemplateService().generateSelectQuestion(sqlType, skillType, database, table)
                    sql += "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                    questTitle = temp['question']
                    questAnswer = temp['answer']
                    queryParams.append(questTitle)
                    queryParams.append(name)
                    queryParams.append(database)
                    queryParams.append(i+1)
                    queryParams.append(type)
                    queryParams.append(endDate)
                    queryParams.append(status)
                    queryParams.append(startTime)
                    queryParams.append(endTime)
                    queryParams.append(questAnswer)
                    queryParams.append(point)
                    queryParams.append(standard)
                    queryParams.append(subject_id)

                print("QUERY PARAMS ---> ",queryParams)
                cursor.execute(sql, queryParams)

            else:
                sql += "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                queryParams = (title,name,database,num,type,endDate,status,startTime,endTime,answer,point,standard,subject_id)
                cursor.execute(sql,queryParams)
                print(dict(cursor.__dict__))

            if cursor.rowcount > 0 :
                results['question_id'] = cursor.lastrowid
                results['message'] = 'add Question success'
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('QuestionService addQuestion() Exception[1] ---> ', e)
            print('QuestionService addQuestion() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))

            error = e.__dict__
            if error['errno'] == 1062 :
                raise ResponseError(HTTP_ERROR_CODE,ERROR_QUESTION_UNIQUE_MSG)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def updateQuestion(self, questionId,name,title, database, num, type, endDate, startTime, endTime,answer,point,standard,subject_id):
        connector = DBConnector()
        try:
            sql = "UPDATE `questions` SET" \
                  "`question_name`=%s, " \
                  "`question_title`=%s, " \
                  "`question_database` =%s, " \
                  "`question_num` =%s, " \
                  "`question_type` =%s, " \
                  "`question_end_date` =%s, " \
                  "`question_start_time` =%s, " \
                  "`question_end_time` =%s, " \
                  "`question_answer` =%s, " \
                  "`question_point` =%s, " \
                  "`question_standard` =%s, " \
                  "`subject_id` =%s WHERE question_id=%s"
            queryParams = (name, title, database, num, type, endDate, startTime, endTime, answer, point, standard, subject_id,questionId)
            cursor = connector.getConnection()
            cursor.execute(sql, queryParams)
            print(dict(cursor.__dict__))
            results = {}
            results['question_id'] = questionId
            results['message'] = 'update Question success'
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('QuestionService updateQuestion() Exception[1] ---> ', e)
            print('QuestionService updateQuestion() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))

            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def deleteQuestion(self,questionId):
        connector = DBConnector()
        try:
            sql = "DELETE FROM questions WHERE question_id=%s"
            queryParams = (questionId,)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            print(dict(cursor.__dict__))
            results = {}
            results['message'] = 'delete question success'
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('QuestionService deleteQuestion() Exception[1] ---> ', e)
            print('QuestionService deleteQuestion() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))

            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()