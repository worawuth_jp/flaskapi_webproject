import json

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE
from constants.users import ERROR_USERNAME_UNIQUE_MSG
from models.responses.ResponseError import ResponseError
from utils.ResponseUtil import ResponseUtil


class AdminService:
    def getAllAdmin(self):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM admin INNER JOIN users ON users.user_id = admin.user_id"
            cursor = connector.getConnection()
            cursor.execute(sql)
            results = []
            for row in cursor.fetchall():
                results.append(row)
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('StudentsService getListsStudents() Exception[1] ---> ', e)
            print('StudentsService getListsStudents() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def getAdminByUserId(self,userId):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM admin INNER JOIN users ON users.user_id = admin.user_id WHERE users.user_id=%s"
            cursor = connector.getConnection()
            queryParams = (userId,)
            cursor.execute(sql,queryParams)
            row = cursor.fetchone()
            return row
        except Exception as e:
            print('StudentsService getListsStudents() Exception[1] ---> ', e)
            print('StudentsService getListsStudents() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def addAdmin(self, username, password, prename, firstname, lastname, email):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "INSERT INTO users(username, password,prename, firstname, lastname, email) VALUES (%s,%s,%s,%s,%s,%s)"
            queryParams = (username,password,prename,firstname,lastname,email)
            cursor.execute(sql,queryParams)
            print(dict(cursor.__dict__))
            results = {}
            if cursor.rowcount > 0 :
                sql = "INSERT INTO admin(user_id) VALUES (%s)"
                queryParams = (cursor.lastrowid,)
                results['user_id'] = cursor.lastrowid
                cursor.execute(sql, queryParams)
                if cursor.rowcount > 0 :
                    results['message'] = 'add admin success'
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('AdminService addAdmin() Exception[1] ---> ', e)
            print('AdminService addAdmin() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))

            error = e.__dict__
            if error['errno'] == 1062 :
                raise ResponseError(HTTP_ERROR_CODE,ERROR_USERNAME_UNIQUE_MSG)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def updateAdmin(self,userId, username, password, prename, firstname, lastname, email):
        connector = DBConnector()
        try:
            sql = "UPDATE users SET username=%s,password=%s,prename=%s,firstname=%s,lastname=%s,email=%s WHERE user_id=%s"
            queryParams = (username,password,prename,firstname,lastname,email,userId)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            print(dict(cursor.__dict__))
            results = {}
            results['user_id'] = userId
            cursor.execute(sql, queryParams)
            results['message'] = 'update admin success'
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('AdminService updateAdmin() Exception[1] ---> ', e)
            print('AdminService updateAdmin() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))

            error = e.__dict__
            if error['errno'] == 1062 :
                raise ResponseError(HTTP_ERROR_CODE,ERROR_USERNAME_UNIQUE_MSG)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def deleteAdmin(self,userId):
        connector = DBConnector()
        try:
            sql = "DELETE FROM users WHERE user_id=%s"
            queryParams = (userId,)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            print(dict(cursor.__dict__))
            results = {}
            results['message'] = 'delete admin success'
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('AdminService deleteAdmin() Exception[1] ---> ', e)
            print('AdminService deleteAdmin() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))

            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()