import json

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE
from models.responses.ResponseError import ResponseError
from utils.ResponseUtil import ResponseUtil


class TeachersService:

    def getListAllTeacher(self):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM teachers " \
                  "INNER JOIN users ON users.user_id = teachers.user_id "
            cursor = connector.getConnection()
            cursor.execute(sql)
            results = []
            rows = cursor.fetchall()
            for row in rows:
                sql = "SELECT DISTINCT subject_id FROM subject_groups WHERE user_id=%s"
                queryParams = (row['user_id'],)
                cursor.execute(sql, queryParams)
                row['subject'] = []
                for subjects in cursor.fetchall():
                    sql = "SELECT * FROM subjects WHERE subject_id=%s"
                    queryParams = (subjects['subject_id'],)
                    cursor.execute(sql,queryParams)

                    for subject in cursor.fetchall():
                        sql = "SELECT * FROM subject_groups WHERE user_id=%s AND subject_id =%s "
                        queryParams = (row['user_id'],subjects['subject_id'])
                        cursor.execute(sql, queryParams)
                        subject['subject_groups'] = []
                        for subject_group in cursor.fetchall():
                            subject['subject_groups'].append(subject_group)
                        row['subject'].append(subject)
                results.append(row)
            return results
        except Exception as e:
            print('TeachersService getListAllTeacher() Exception ---> ', e)
            print('TeachersService getListAllTeacher() Exception ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def getTeacher(self,userId):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM teachers " \
                  "INNER JOIN users ON users.user_id = teachers.user_id WHERE users.user_id=%s"
            cursor = connector.getConnection()
            queryParams = (userId,)
            cursor.execute(sql,queryParams)
            results = {}
            rows = cursor.fetchall()
            for row in rows:
                sql = "SELECT DISTINCT subject_id FROM subject_groups WHERE user_id=%s"
                queryParams = (row['user_id'],)
                cursor.execute(sql, queryParams)
                row['subject'] = []
                for subjects in cursor.fetchall():
                    sql = "SELECT * FROM subjects WHERE subject_id=%s"
                    queryParams = (subjects['subject_id'],)
                    cursor.execute(sql, queryParams)
                    for subject in cursor.fetchall():
                        sql = "SELECT * FROM subject_groups WHERE user_id=%s AND subject_id =%s "
                        queryParams = (row['user_id'], subjects['subject_id'])
                        cursor.execute(sql, queryParams)
                        subject['subject_groups'] = []
                        for subject_group in cursor.fetchall():
                            subject['subject_groups'].append(subject_group)
                        row['subject'].append(subject)
                results = row
            return results
        except Exception as e:
            print('TeachersService getListAllTeacher() Exception ---> ', e)
            print('TeachersService getListAllTeacher() Exception ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def searchTeacher(self,text):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM teachers " \
                  "INNER JOIN users ON users.user_id = teachers.user_id " \
                  "WHERE users.username LIKE %s OR users.firstname LIKE %s OR users.lastname LIKE %s"
            cursor = connector.getConnection()
            textSearch = "%"+text+"%"
            queryParams = (textSearch,textSearch,textSearch)
            cursor.execute(sql,queryParams)
            results = []
            rows = cursor.fetchall()
            for row in rows:
                sql = "SELECT DISTINCT subject_id FROM subject_groups WHERE user_id=%s"
                queryParams = (row['user_id'],)
                cursor.execute(sql, queryParams)
                row['subject'] = []
                for subjects in cursor.fetchall():
                    sql = "SELECT * FROM subjects WHERE subject_id=%s"
                    queryParams = (subjects['subject_id'],)
                    cursor.execute(sql, queryParams)
                    for subject in cursor.fetchall():
                        sql = "SELECT * FROM subject_groups WHERE user_id=%s AND subject_id =%s "
                        queryParams = (row['user_id'], subjects['subject_id'])
                        cursor.execute(sql, queryParams)
                        subject['subject_groups'] = []
                        for subject_group in cursor.fetchall():
                            subject['subject_groups'].append(subject_group)
                        row['subject'].append(subject)
                results.append(row)

            return results
        except Exception as e:
            print('TeachersService getListAllTeacher() Exception ---> ', e)
            print('TeachersService getListAllTeacher() Exception ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def addTeacher(self,teacherId,prename,firstname,lastname,email):
        connector = DBConnector()
        try:
            username = lastname+'_'+firstname[0]
            password = username
            sql = "INSERT INTO users(username,password,prename,firstname,lastname,email) VALUES(%s,%s,%s,%s,%s,%s)"
            queryParams = (username,password,prename,firstname,lastname,email)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            result = {}

            if cursor.rowcount > 0 :
                sql = "INSERT INTO teachers(user_id,teacher_id) VALUES(%s,%s)"
                userId = cursor.lastrowid
                queryParams = (userId,teacherId)
                result['user_id'] = userId
                cursor.execute(sql,queryParams)
                if cursor.rowcount > 0 :
                    result['teacher_id'] = teacherId
                    result['message'] = 'add teacher success'

            return result

            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

        except Exception as e:
            print(e)
            if e.errno == 1062 or e.sqlstate == 230000:
                raise ResponseError(400, "มีชื่อผู้ใช้นี้ในระบบแล้ว")
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def updateTeacher(self, userId, teacherId, prename, firstname, lastname, email):
        connector = DBConnector()
        try:
            username = lastname + '_' + firstname[0]
            password = username
            sql = "UPDATE users SET username=%s,password=%s,prename=%s,firstname=%s,lastname=%s,email=%s WHERE user_id=%s"
            queryParams = (username, password, prename, firstname, lastname, email,userId)
            cursor = connector.getConnection()
            cursor.execute(sql, queryParams)

            sql = "UPDATE teachers SET teacher_id=%s WHERE user_id=%s"
            queryParams = (teacherId, userId)
            cursor.execute(sql, queryParams)
            result = {}
            result['user_id'] = userId
            result['teacher_id'] = teacherId
            result['message'] = 'update teacher success'
            return result

        except Exception as e:
            print(e)
            print('TeachersService updateTeacher() Exception ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def deleteTeacher(self, userId):
        connector = DBConnector()
        try:
            sql = "DELETE FROM users WHERE user_id=%s"
            queryParams = (userId,)
            cursor = connector.getConnection()
            cursor.execute(sql, queryParams)

            result = {}
            result['message'] = 'delete teacher success'
            return result

        except Exception as e:
            print(e)
            print('TeachersService updateTeacher() Exception ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()
