import json

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from constants.users import TYPE_ADMIN, TYPE_TEACHER, TYPE_STUDENT, ERROR_GENERATE_MSG
from models.responses.ResponseError import ResponseError
from services.StudentsService import StudentsService
from services.TeachersService import TeachersService
from utils.ResponseUtil import ResponseUtil

class UserServices :
    def getAllUsers(self):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM users"
            cursor = connector.getConnection()
            cursor.execute(sql)
            users = []
            for row in cursor.fetchall():
                users.append(row)

            results = {}
            results['users'] = users

            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ",json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('UserServices getAllUsers() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def loginUser(self,typeUser,username,password) :
        connector = DBConnector()
        try:
            cursor = connector.getConnection()
            sql = ""
            queryParams = ()
            username = username.lower()
            if typeUser.upper() == TYPE_ADMIN:
                sql = "SELECT * FROM users INNER JOIN admin ON admin.user_id = users.user_id WHERE username=%s AND password=%s"
                queryParams = queryParams + (username,password)

            elif typeUser.upper() == TYPE_TEACHER :
                sql = "SELECT * FROM users INNER JOIN teachers ON users.user_id = teachers.user_id WHERE username=%s AND password=%s"
                queryParams = queryParams + (username, password)
            elif typeUser.upper() == TYPE_STUDENT :
                sql = "SELECT * FROM users INNER JOIN students ON users.user_id = students.user_id WHERE username=%s AND password=%s"
                queryParams = queryParams + (username,password)

            #print(queryParams)
            cursor.execute(sql,queryParams)

            results = {}
            print(cursor)
            for row in cursor.fetchall():
                print(dict(row))
                results = dict(row)
                results['type_user'] = typeUser

            results['isLogin'] = True if len(results) > 0 else False

            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ",json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('UserServices loginUser() Exception ---> ',e)
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def userGenerate(self, typeUser,id):
        connector = DBConnector()
        try:
            cursor = connector.getConnection()
            sql = "SELECT * FROM users"
            queryParams = ()
            if typeUser.upper() == TYPE_TEACHER :
                sql += "INNER JOIN teachers ON teachers.user_id = users.user_id WHERE teachers.teacher_id= %s"
                queryParams = queryParams + (id,)
            elif typeUser.upper() == TYPE_STUDENT :
                sql += "INNER JOIN students ON students.user_id = users.user_id WHERE teachers.teacher_id= %s"
                queryParams = queryParams + (id,)

            cursor.execute(sql, queryParams)
            username = ""
            password = ""
            for row in cursor.fetchall():
                if typeUser.upper() == TYPE_TEACHER:
                    username = row['lastname']+'_'+row['firstname'][0]
                elif typeUser.upper() == TYPE_STUDENT:
                    username = row['student_id']
                username = username.lower()
                password = username.lower()

            print("Username And Password ---> [",username,',',password,']')
            sql = ""
            queryParams = ()
            resultSuccess = {}
            if typeUser.upper() == TYPE_TEACHER:
                resultSuccess = TeachersService().addTeacher()
            elif typeUser.upper() == TYPE_STUDENT:
                resultSucces = StudentsService().addStudent(id,)


            return ResponseError(HTTP_BAD_REQUEST_CODE, ERROR_GENERATE_MSG)
        except Exception as e:
            print('UserServices userGenerate() Exception ---> ', e)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()