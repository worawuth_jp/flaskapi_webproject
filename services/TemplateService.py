import json
import random
from datetime import datetime

from models.templates.TemplateDecoder import TemplateDecoder

now = datetime.now() # current date and time

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE, HTTP_SUCCESS_CODE
from constants.template import ERROR_INSERT_TEMPLATE_MSG, ERROR_UPDATE_TEMPLATE_MSG, productNameSet, custAddrSet, \
    custNameSet, custProvinceSet, custPhoneSet
from models.responses.ResponseError import ResponseError
from utils.ResponseUtil import ResponseUtil

class TemplateService :
    def getAllTemplate(self):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM templates"
            cursor = connector.getConnection()
            cursor.execute(sql)
            templates = []
            for row in cursor.fetchall():
                templates.append(row)

            responseSuccess = ResponseUtil().transformResponseWithData(templates)
            print("RESPONSE ---> ",json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('TemplateService getAllTemplate() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def createTemplate(self,database,question,answer,skillType,sqlType):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "INSERT INTO templates(template_database,template_quest,template_answer,template_skill_type,template_sql_type) VALUES(%s,%s,%s,%s,%s)"
            queryParams = (database,question,answer,skillType,sqlType)

            cursor.execute(sql,queryParams)
            if cursor.rowcount > 0 :
                responseSuccess = ResponseUtil().transformResponse()
                print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
                return responseSuccess
            print('RECORD NOT SAVE')
            return ResponseError(HTTP_BAD_REQUEST_CODE, ERROR_INSERT_TEMPLATE_MSG)
        except Exception as e:
            print(e)
            print('TemplateService createTemplate() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def updateTemplate(self,tempId,database,question,answer,skillType,sqlType):
        connector = DBConnector()
        try:
            sql = "UPDATE templates SET template_database=%s,template_quest=%s,template_answer=%s,template_skill_type=%s,template_sql_type=%s WHERE template_id=%s"
            queryParams = (database,question,answer,skillType,sqlType,tempId)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)

            responseSuccess = ResponseUtil().transformResponse()
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print(e)
            if hasattr(e, 'code') :
                raise e
            print('TemplateService updateTemplate() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def deleteTemplate(self,tempId):
        connector = DBConnector()
        try:
            sql = "DELETE FROM templates WHERE template_id=%s"
            queryParams = (tempId,)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)

            responseSuccess = ResponseUtil().transformResponse()
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess

        except Exception as e:
            print(e)
            if hasattr(e, 'code') :
                raise e
            print('TemplateService deleteTemplate() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def searchByType(self,skillType,sqlType):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM templates WHERE 1=1"
            queryParams = ()
            if skillType :
                sql += " AND template_skill_type LIKE %s"
                skill = '%'+skillType+'%'
                queryParams += (skill.upper(),)
            if sqlType :
                sql += " AND template_sql_type LIKE %s"
                sqlType_new = '%'+sqlType+'%'
                queryParams += (sqlType_new.upper(),)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            data = []
            for row in cursor.fetchall():
                data.append(row)
            print("data ---> ", json.dumps(data))
            responseSuccess = ResponseUtil().transformResponseWithData(data)
            print(json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print(e)
            print('TemplateService searchByType() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def selectSearchByType(self,skillType,sqlType):
        connector = DBConnector()
        try:
            sql = "SELECT COUNT(*) AS NUM FROM templates WHERE 1=1"
            queryParams = ()
            if skillType :
                sql += " AND template_skill_type LIKE %s"
                skill = '%'+skillType+'%'
                queryParams += (skill.upper(),)
            if sqlType :
                sql += " AND template_sql_type LIKE %s"
                sqlType_new = '%'+sqlType+'%'
                queryParams += (sqlType_new.upper(),)
            cursor = connector.getConnection()
            cursor.execute(sql, queryParams)
            count = 0
            for row in cursor.fetchall():
                count = row['NUM']

            sql = "SELECT * FROM templates WHERE 1=1"
            queryParams = ()
            if skillType:
                sql += " AND template_skill_type LIKE %s"
                skill = '%' + skillType + '%'
                queryParams += (skill.upper(),)
            if sqlType:
                sql += " AND template_sql_type LIKE %s"
                sqlType_new = '%' + sqlType + '%'
                queryParams += (sqlType_new.upper(),)

            sql += " LIMIT 1 OFFSET %s"
            count2 = random.randint(0, count - 1)
            print(count2)
            queryParams += (count2,)
            cursor.execute(sql, queryParams)
            data = None
            print(sql)
            for row in cursor.fetchall():
                #print("data [] ---> ", json.dumps(row))
                data = row
            print("data ---> ", json.dumps(data))
            responseSuccess = data
            print(json.dumps(responseSuccess))
            return responseSuccess
        except Exception as e:
            print(e)
            print('TemplateService searchByType() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def generateSelectQuestion(self,cmd,skillType,database,table):
        connector = DBConnector()
        filedExtraList = ['UPPER', 'LOWER', 'AVG', 'MIN', 'MAX', 'COUNT', 'SUM', 'DISTINCT','FILED_NAME']
        oprRandList = ["=", ">", "<", "<>",">=","<=","BETWEEN","LIKE","IN"]
        optionalList = ["WHERE", "GROUP BY", "ORDER BY"]
        try:
            sql = "SELECT template_answer FROM template WHERE template_command = %s LIMIT 1"
            queryParams = [cmd]
            cursor = connector.getConnection()
            cursor.execute(sql, queryParams)
            row = cursor.fetchone()
            self.data = row['template_answer']
            self.template = ""
            sql = "select TABLE_NAME,COLUMN_NAME from information_schema.columns " \
                  "where table_schema = %s and TABLE_NAME = %s order by table_name,ordinal_position"
            queryParams = [database, table]
            cursor.execute(sql, queryParams)
            listResult = []
            for row in cursor.fetchall():
                listResult.append(row)

            filedNum = len(listResult)
            self.template = self.data.replace("[remember|understand|implement|analysis]", skillType.upper())

            if cmd.upper() == "SELECT":
                column = "<COLUMN>*|<OPTION><UPPER><FILED_NAME/>[<NAME/>]</UPPER>|<LOWER><FILED_NAME/>[<NAME/>]</LOWER>|<AVG><FILED_NAME/>[<NAME/>]</AVG>|<MIN><FILED_NAME/>[<NAME/>]</MIN>|<MAX><FILED_NAME/>[<NAME/>]</MAX>|<COUNT>*|<FILED_NAME/>[<NAME/>]</COUNT>|<SUM><FILED_NAME/>[<NAME/>]</SUM>|<FILED_NAME><VALUE/>[<NAME/>]</FILED_NAME>|<DISTINCT><FILED_NAME/>[<NAME/>]</DISTINCT></OPTION></COLUMN>"
                index = random.randint(0, 1)
                if index == 0:
                    self.template = self.template.replace(column, "<COLUMN>*</COLUMN>")
                else:
                    numFiled = random.randint(1,filedNum)
                    listFiled = []
                    colList = []

                    # RAND FILED COLUMN OF TEMPLATE
                    i = numFiled
                    while i > 0:
                        print("LOOP")
                        iRand = random.randint(0, len(filedExtraList) - 1)
                        if filedExtraList[iRand] not in colList or filedExtraList[iRand] == "FILED_NAME":
                            colList.append(filedExtraList[iRand])
                            i -= 1

                    i = numFiled
                    breakNum = i*2
                    # Rand Filed IN DB
                    while i > 0 and breakNum > 0 :
                        print("LOOP2")
                        print("COLUMN NAME == ",colList[numFiled-i])
                        iRand = random.randint(0,filedNum-1)
                        if listResult[iRand]['COLUMN_NAME'] not in listFiled:
                            sql = "select COUNT(*) AS NUM from information_schema.columns " \
                            "where table_schema = %s and TABLE_NAME = %s and COLUMN_NAME = %s and COLUMN_COMMENT LIKE %s order by table_name,ordinal_position"
                            queryParams = [database,table,listResult[iRand]['COLUMN_NAME'],'%'+colList[numFiled-i]+'%']
                            cursor.execute(sql,queryParams)
                            row = cursor.fetchone()
                            print("COUNT : ",row['NUM'])
                            if row['NUM'] > 0:
                                listFiled.append(listResult[iRand]['COLUMN_NAME'])
                                i -= 1
                            elif colList[numFiled-i] in ["FILED_NAME","DISTINCT"]:
                                listFiled.append(listResult[iRand]['COLUMN_NAME'])
                                i -= 1
                            else:
                                while True:
                                    indexRand = random.randint(0,len(filedExtraList)-1)
                                    if colList[numFiled - i] != filedExtraList[indexRand] and filedExtraList[indexRand] not in colList:
                                        colList[numFiled - i] = filedExtraList[indexRand]
                                        break

                    new_column = ""
                    new_column += "<COLUMN><OPTION>"
                    for item in colList:
                        isName = False
                        if random.randint(0,1) == 1:
                            isName = True

                        if item == "FILED_NAME":
                            new_column += "<" + item + ">"
                            new_column += listFiled[colList.index(item)]
                            new_column += "</" + item + ">"
                        else:
                            new_column += "<" + item +">"
                            new_column += "<FILED_NAME>"+listFiled[colList.index(item)]+"</FIELD_NAME>"
                            if isName:
                                new_column += "<NAME>"+listFiled[colList.index(item)].upper()+"</NAME>"
                            new_column += "</" + item + ">"
                    new_column += "</OPTION></COLUMN>"

                    print("LIST FILED : ",listFiled)
                    print("LIST COL TEMP : ",colList)

                    self.template = self.template.replace(column,new_column)

                optional = "[<OPTION [SEQ]><WHERE><CON>[<PRE>AND|OR</PRE>]<FILED_NAME/><OPERATOR>=|>|<|<>|>=|<=|LIKE|BETWEEN|IN</OPERATOR><VALUE/>[<VALUE2/>][<VALUES/>]</CON></WHERE>|(<GROUPBY><FILED_NAME/></GROUPBY>|<HAVING><CON>[<PRE>AND|OR</PRE>]<FILED_NAME/>|[<AVG/>|<MIN/>|<MAX/>|<COUNT/>|<SUM/>]<OPERATOR>=|<|<>|>|>=|<=|LIKE|BETWEEN</OPERATOR><VALUE/>[<VALUE2/>]</CON><HAVING/>)|<ORDERBY><FILED_NAME/><VALUE>ASC|DESC</VALUE></ORDERBY></OPTION>]"

                nOpional = []
                numOptional = random.randint(1,len(optionalList)-1)
                i = numOptional
                while i > 0:
                    print("LOOP3")
                    randI = random.randint(0, len(optionalList) - 1)
                    if optionalList[randI] not in nOpional:
                        nOpional.append(optionalList[randI])
                        i -= 1
                new_option = ""
                if random.randint(0,1) == 1:
                    new_option = "<OPTION>"
                    if "WHERE" in nOpional:
                        whereTemp = "<WHERE>"
                        numCon = random.randint(1,filedNum)
                        filedConList = []
                        i = numCon
                        while i > 0:
                            print("LOOP5")
                            if listResult[numCon-i]["COLUMN_NAME"] not in filedConList:
                                filedConList.append(listResult[numCon-i]["COLUMN_NAME"])
                                i -= 1
                        i = numCon
                        while i > 0:
                            print("LOOP6")
                            whereTemp += "<CON>"
                            if i != numCon:
                                whereTemp += "<PRE>"
                                if random.randint(0, 1) == 1:
                                    whereTemp += "AND"
                                else:
                                    whereTemp += "OR"
                                whereTemp += "</PRE>"
                            whereTemp += "<FILED_NAME>"+filedConList[numCon-i]+"</FILED_NAME>"
                            opr = ""
                            oprRandI = -1
                            while True:
                                oprRandI = random.randint(0, 8)
                                sql = "select COUNT(*) AS NUM from information_schema.columns " \
                                      "where table_schema = %s and TABLE_NAME = %s and COLUMN_NAME = %s and COLUMN_COMMENT LIKE %s order by table_name,ordinal_position"
                                queryParams = [database, table, filedConList[numCon-i],
                                               '%' + oprRandList[oprRandI] + '%']
                                cursor.execute(sql, queryParams)
                                row = cursor.fetchone()
                                if row['NUM'] > 0:
                                    break

                            whereTemp += "<OPERATOR>&"+oprRandList[oprRandI]+"&</OPERATOR>"
                            if oprRandI == 6:
                                sql = "SELECT "+filedConList[numCon - i]+" FROM "+table+" ORDER BY RAND() LIMIT 2"
                                cursor.execute(sql)
                                word = []
                                for row in cursor.fetchall():
                                    word.append(row[filedConList[numCon - i]])
                                whereTemp += "<VALUE>"+str(min(word))+"</VALUE><VALUE2>"+str(max(word))+"</VALUE2>"
                            elif oprRandI == 7:
                                word = ""
                                if random.randint(0,1) == 1:
                                    word += "%"
                                sql = "SELECT "+filedConList[numCon - i]+" FROM "+table+" ORDER BY RAND() LIMIT 1"
                                cursor.execute(sql)
                                row = cursor.fetchone()
                                word += row[filedConList[numCon-i]]
                                if random.randint(0,1) == 1:
                                    word += "%"
                                whereTemp += "<VALUE>"+str(word)+"</VALUE>"
                            elif oprRandI == 8:
                                num = random.randint(1,8)
                                sql = "SELECT "+filedConList[numCon - i]+" FROM "+table+" ORDER BY RAND() LIMIT "+str(num)
                                cursor.execute(sql)
                                word = ""
                                for row in cursor.fetchall():
                                    word += "<VALUE>"+str(row[filedConList[numCon - i]])+"</VALUE>"
                                whereTemp += "<VALUES>" + str(word) + "</VALUES>"
                            whereTemp += "</CON>"
                            i -= 1
                        whereTemp += "</WHERE>"
                        new_option += whereTemp
                    if "GROUP BY" in nOpional:
                        n = random.randint(1,filedNum)
                        filedGroupList = []
                        groupByTemp = "<GROUPBY>"
                        i = n
                        while i > 0:
                            print("LOOP7")
                            randI = random.randint(0,len(listResult)-1)
                            if listResult[randI]["COLUMN_NAME"] not in filedGroupList:
                                filedGroupList.append(listResult[randI])
                                groupByTemp += "<FILED_NAME>"+listResult[randI]["COLUMN_NAME"]+"</FILED_NAME>"
                                i -= 1
                        groupByTemp += "</GROUPBY>"
                        new_option += groupByTemp
                        if random.randint(0,1) == 1:
                            havingTemp = "<HAVING>"
                            whereTemp = ""
                            numCon = random.randint(1, filedNum)
                            filedConList = []
                            i = numCon
                            while i > 0:
                                print("LOOP8")
                                if listResult[numCon - i]["COLUMN_NAME"] not in filedConList:
                                    filedConList.append(listResult[numCon - i]["COLUMN_NAME"])
                                    i -= 1
                            i = numCon
                            while i > 0:
                                print("LOOP9")
                                whereTemp += "<CON>"
                                if i != numCon:
                                    whereTemp += "<PRE>"
                                    if random.randint(0, 1) == 1:
                                        whereTemp += "AND"
                                    else:
                                        whereTemp += "OR"
                                    whereTemp += "</PRE>"
                                whereTemp += "<FILED_NAME>" + filedConList[numCon - i] + "</FILED_NAME>"
                                opr = ""
                                oprRandI = -1
                                print("IIII2")
                                while True:
                                    oprRandI = random.randint(0, 8)
                                    sql = "select COUNT(*) AS NUM from information_schema.columns " \
                                          "where table_schema = %s and TABLE_NAME = %s and COLUMN_NAME = %s and COLUMN_COMMENT LIKE %s order by table_name,ordinal_position"
                                    queryParams = [database, table, filedConList[numCon - i],
                                                   '%' + oprRandList[oprRandI] + '%']
                                    cursor.execute(sql, queryParams)
                                    row = cursor.fetchone()
                                    print("IIII")
                                    if row['NUM'] > 0:
                                        break

                                whereTemp += "<OPERATOR>&" + oprRandList[oprRandI] + "&</OPERATOR>"
                                if oprRandI == 6:
                                    sql = "SELECT " + filedConList[
                                        numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT 2"
                                    cursor.execute(sql)
                                    word = []
                                    for row in cursor.fetchall():
                                        word.append(row[filedConList[numCon - i]])
                                    whereTemp += "<VALUE>" + str(min(word)) + "</VALUE><VALUE2>" + str(
                                        max(word)) + "</VALUE2>"
                                elif oprRandI == 7:
                                    word = ""
                                    if random.randint(0, 1) == 1:
                                        word += "%"
                                    sql = "SELECT " + filedConList[
                                        numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT 1"
                                    cursor.execute(sql)
                                    row = cursor.fetchone()
                                    word += row[filedConList[numCon - i]]
                                    if random.randint(0, 1) == 1:
                                        word += "%"
                                    whereTemp += "<VALUE>" + str(word) + "</VALUE>"
                                elif oprRandI == 8:
                                    num = random.randint(1, 8)
                                    sql = "SELECT " + filedConList[
                                        numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT " + str(num)
                                    cursor.execute(sql)
                                    word = ""
                                    for row in cursor.fetchall():
                                        word += "<VALUE>" + str(row[filedConList[numCon - i]]) + "</VALUE>"
                                    whereTemp += "<VALUES>" + str(word) + "</VALUES>"
                                whereTemp += "</CON>"
                                i -= 1
                            havingTemp += whereTemp
                            havingTemp += "</HAVING>"
                            new_option += havingTemp
                            print("HAVING ",havingTemp)

                    if "ORDER BY" in nOpional:
                        orderByTemp = "<ORDERBY>"
                        numOrderFiled = random.randint(1,filedNum)
                        orderFiledList = []
                        values = ["ASC","DESC"]
                        i = numOrderFiled
                        while i>0:
                            randI = random.randint(0,len(listResult)-1)
                            if listResult[randI] not in orderFiledList:
                                orderByTemp += "<FILED>"
                                orderFiledList.append(listResult[randI])
                                orderByTemp += "<FILED_NAME>"+str(listResult[randI]["COLUMN_NAME"])+"</FILED_NAME>"
                                orderByTemp += "<VALUE>"+values[random.randint(0,1)]+"</VALUE>"
                                orderByTemp += "</FILED>"
                                i -= 1
                        orderByTemp += "</ORDERBY>"
                        new_option += orderByTemp

                    new_option += "</OPTION>"
                    print("OPTION : ",new_option)
                self.template = self.template.replace(optional,new_option)

            if cmd.upper() == "INSERT":
                column = "<COLUMN><OPTION><FILED_NAME><VALUE/>[<NAME/>]</FILED_NAME></OPTION></COLUMN>"
                new_column = "<COLUMN><OPTION>"
                for filed in listResult:
                    new_column += "<FILED_NAME>"+filed["COLUMN_NAME"]+"</FILED_NAME>"
                new_column += "</OPTION></COLUMN>"
                self.template = self.template.replace(column,new_column)

                values = "<VALUES>"
                sql = "SELECT * FROM "+table+" ORDER BY "+listResult[0]["COLUMN_NAME"] + " DESC"
                print(sql)
                cursor.execute(sql)
                row = cursor.fetchone()
                num = int(row[listResult[0]["COLUMN_NAME"]])+1
                strNum = ""
                if num < 10:
                    strNum = "00"+str(num)
                elif num < 100:
                    strNum = "0"+str(num)
                elif num < 100:
                    strNum = str(num)

                if table != "product":
                    strNum = table[0:2].upper()+"-"+strNum
                for fileds in listResult:
                    filed = fileds["COLUMN_NAME"]
                    values += "<VALUE>"
                    if table == "product":
                        if filed =="prodNo":
                            values += strNum
                        if filed == "prodName":
                            values += productNameSet[random.randint(0,len(productNameSet)-1)]
                        if filed == "prodPrice":
                            values += str(random.randint(1,999))
                        if filed == "prodTotal":
                            values += str(random.randint(1,100))

                    elif table == "customer":
                        if filed =="custID":
                            values += strNum
                        if filed == "custName":
                            values += custNameSet[random.randint(0,len(custNameSet)-1)]
                        if filed == "custAddr":
                            values += custAddrSet[random.randint(0,len(custAddrSet)-1)]
                        if filed == "custProvince":
                            values += custProvinceSet[random.randint(0,len(custProvinceSet)-1)]
                        if filed == "custPhone":
                            values += custPhoneSet[random.randint(0,len(custPhoneSet)-1)]

                    elif table == "orders":
                        if filed =="orderNo":
                            values += strNum
                        if filed == "orderDate":
                            date_time = now.strftime("%Y-%m-%d")
                            values += date_time
                        if filed == "orderTotal":
                            values += str(random.randint(1,100))
                        if filed == "custID":
                            sql = "SELECT * FROM customer ORDER BY RAND() LIMIT 1"
                            cursor.execute(sql)
                            row = cursor.fetchone()
                            values += row['custID']

                    elif table == "order_details":
                        if filed =="prodNo":
                            sql = "SELECT * FROM product ORDER BY RAND() LIMIT 1"
                            cursor.execute(sql)
                            row = cursor.fetchone()
                            values += row['prodNo']
                        if filed == "orderNo":
                            sql = "SELECT * FROM orders ORDER BY RAND() LIMIT 1"
                            cursor.execute(sql)
                            row = cursor.fetchone()
                            values += row['orderNo']
                        if filed == "quantity":
                            values += str(random.randint(1,100))
                    values += "</VALUE>"
                values += "</VALUES>"
                self.template = self.template.replace("<VALUES/>",values)

            if cmd.upper() == "UPDATE":
                values = "<VALUES>"
                for fileds in listResult:
                    filed = fileds["COLUMN_NAME"]
                    sql = "SELECT * FROM "+table+" ORDER BY RAND() LIMIT 1"
                    cursor.execute(sql)
                    row = cursor.fetchone()
                    values += "<VALUE name='"+filed+"'>"+str(row[filed])+"(new)</VALUE>"
                values += "</VALUES>"
                whereTempOld = "<WHERE><CON>[<PRE>AND|OR</PRE>]<FILED_NAME/><OPERATOR>=|>|<|<>|>=|<=|LIKE|BETWEEN|IN</OPERATOR><VALUE/>[<VALUE2/>][<VALUES/>]</CON></WHERE>"
                self.template = self.template.replace(whereTempOld, "<WHERE/>")
                self.template = self.template.replace("<VALUES/>",values)

                whereTemp = "<WHERE>"
                numCon = random.randint(1, filedNum)
                filedConList = []
                i = numCon
                while i > 0:
                    print("LOOP5")
                    if listResult[numCon - i]["COLUMN_NAME"] not in filedConList:
                        filedConList.append(listResult[numCon - i]["COLUMN_NAME"])
                        i -= 1
                i = numCon
                while i > 0:
                    print("LOOP6")
                    whereTemp += "<CON>"
                    if i != numCon:
                        whereTemp += "<PRE>"
                        if random.randint(0, 1) == 1:
                            whereTemp += "AND"
                        else:
                            whereTemp += "OR"
                        whereTemp += "</PRE>"
                    whereTemp += "<FILED_NAME>" + filedConList[numCon - i] + "</FILED_NAME>"
                    opr = ""
                    oprRandI = -1
                    while True:
                        oprRandI = random.randint(0, 8)
                        sql = "select COUNT(*) AS NUM from information_schema.columns " \
                              "where table_schema = %s and TABLE_NAME = %s and COLUMN_NAME = %s and COLUMN_COMMENT LIKE %s order by table_name,ordinal_position"
                        queryParams = [database, table, filedConList[numCon - i],
                                       '%' + oprRandList[oprRandI] + '%']
                        cursor.execute(sql, queryParams)
                        row = cursor.fetchone()
                        if row['NUM'] > 0:
                            break

                    whereTemp += "<OPERATOR>&" + oprRandList[oprRandI] + "&</OPERATOR>"
                    if oprRandI == 6:
                        sql = "SELECT " + filedConList[numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT 2"
                        cursor.execute(sql)
                        word = []
                        for row in cursor.fetchall():
                            word.append(row[filedConList[numCon - i]])
                        whereTemp += "<VALUE>" + str(min(word)) + "</VALUE><VALUE2>" + str(max(word)) + "</VALUE2>"
                    elif oprRandI == 7:
                        word = ""
                        if random.randint(0, 1) == 1:
                            word += "%"
                        sql = "SELECT " + filedConList[numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT 1"
                        cursor.execute(sql)
                        row = cursor.fetchone()
                        word += row[filedConList[numCon - i]]
                        if random.randint(0, 1) == 1:
                            word += "%"
                        whereTemp += "<VALUE>" + str(word) + "</VALUE>"
                    elif oprRandI == 8:
                        num = random.randint(1, 8)
                        sql = "SELECT " + filedConList[numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT " + str(
                            num)
                        cursor.execute(sql)
                        word = ""
                        for row in cursor.fetchall():
                            word += "<VALUE>" + str(row[filedConList[numCon - i]]) + "</VALUE>"
                        whereTemp += "<VALUES>" + str(word) + "</VALUES>"
                    whereTemp += "</CON>"
                    i -= 1
                whereTemp += "</WHERE>"
                self.template = self.template.replace("<WHERE/>",whereTemp)

            if cmd.upper() == "DELETE":
                whereTemp = "<WHERE>"
                numCon = random.randint(1, filedNum)
                filedConList = []
                i = numCon
                while i > 0:
                    print("LOOP5")
                    if listResult[numCon - i]["COLUMN_NAME"] not in filedConList:
                        filedConList.append(listResult[numCon - i]["COLUMN_NAME"])
                        i -= 1
                i = numCon
                while i > 0:
                    print("LOOP6")
                    whereTemp += "<CON>"
                    if i != numCon:
                        whereTemp += "<PRE>"
                        if random.randint(0, 1) == 1:
                            whereTemp += "AND"
                        else:
                            whereTemp += "OR"
                        whereTemp += "</PRE>"
                    whereTemp += "<FILED_NAME>" + filedConList[numCon - i] + "</FILED_NAME>"
                    opr = ""
                    oprRandI = -1
                    while True:
                        oprRandI = random.randint(0, 8)
                        sql = "select COUNT(*) AS NUM from information_schema.columns " \
                              "where table_schema = %s and TABLE_NAME = %s and COLUMN_NAME = %s and COLUMN_COMMENT LIKE %s order by table_name,ordinal_position"
                        queryParams = [database, table, filedConList[numCon - i],
                                       '%' + oprRandList[oprRandI] + '%']
                        cursor.execute(sql, queryParams)
                        row = cursor.fetchone()
                        if row['NUM'] > 0:
                            break

                    whereTemp += "<OPERATOR>&" + oprRandList[oprRandI] + "&</OPERATOR>"
                    if oprRandI == 6:
                        sql = "SELECT " + filedConList[numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT 2"
                        cursor.execute(sql)
                        word = []
                        for row in cursor.fetchall():
                            word.append(row[filedConList[numCon - i]])
                        whereTemp += "<VALUE>" + str(min(word)) + "</VALUE><VALUE2>" + str(max(word)) + "</VALUE2>"
                    elif oprRandI == 7:
                        word = ""
                        if random.randint(0, 1) == 1:
                            word += "%"
                        sql = "SELECT " + filedConList[numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT 1"
                        cursor.execute(sql)
                        row = cursor.fetchone()
                        word += row[filedConList[numCon - i]]
                        if random.randint(0, 1) == 1:
                            word += "%"
                        whereTemp += "<VALUE>" + str(word) + "</VALUE>"
                    elif oprRandI == 8:
                        num = random.randint(1, 8)
                        sql = "SELECT " + filedConList[numCon - i] + " FROM " + table + " ORDER BY RAND() LIMIT " + str(
                            num)
                        cursor.execute(sql)
                        word = ""
                        for row in cursor.fetchall():
                            word += "<VALUE>" + str(row[filedConList[numCon - i]]) + "</VALUE>"
                        whereTemp += "<VALUES>" + str(word) + "</VALUES>"
                    whereTemp += "</CON>"
                    i -= 1
                whereTemp += "</WHERE>"
                whereTempOld = "<WHERE><CON>[<PRE>AND|OR</PRE>]<FILED_NAME/><OPERATOR>=|>|<|<>|>=|<=|LIKE|BETWEEN|IN</OPERATOR><VALUE/>[<VALUE2/>][<VALUES/>]</CON></WHERE>"
                self.template = self.template.replace(whereTempOld, whereTemp)

            self.template = self.template.replace("<TABLE_NAME/>","<TABLE_NAME>"+table+"</TABLE_NAME>")

            templateDecoder = TemplateDecoder()
            textDecode = templateDecoder.decode(self.template)

            responseSuccess = textDecode
            return responseSuccess
        except Exception as e:
            print('TemplateService generateSelectQuestion() Exception ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()
