import json

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE
from models.responses.ResponseError import ResponseError
from services.SubjectService import SubjectService
from utils.ResponseUtil import ResponseUtil


class StudentsService:

    def listMajor(self):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM majors"
            cursor = connector.getConnection()
            cursor.execute(sql)
            results = []
            rows = cursor.fetchall()
            for row in rows:
                results.append(row)
            responseSuccess = ResponseUtil().transformResponseWithData(results)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('StudentsService listMajor() Exception[1] ---> ', e)
            print('StudentsService listMajor() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def getListsStudents(self):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM students " \
                  "INNER JOIN users ON users.user_id = students.user_id " \
                  "INNER JOIN majors ON majors.major_id = students.major_id"
            cursor = connector.getConnection()
            cursor.execute(sql)
            results = []
            rows = cursor.fetchall()
            for row in rows:
                sql = "SELECT * FROM student_subject_groups ssg " \
                      "INNER JOIN subject_groups sg ON sg.subject_group_id = ssg.subject_group_id " \
                      "WHERE ssg.user_id=%s AND ssg.enable=%s AND sg.enable=%s"
                queryParams = (row['user_id'],"Y","Y")
                cursor.execute(sql,queryParams)
                row['subject'] = []
                for record in cursor.fetchall():
                    row['subject'].append(SubjectService().getSubject(record['subject_id']))
                results.append(row)
            return results
        except Exception as e:
            print('StudentsService getListsStudents() Exception[1] ---> ', e)
            print('StudentsService getListsStudents() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def getStudent(self,userId,subjectId):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM students " \
                  "INNER JOIN users ON users.user_id = students.user_id " \
                  "INNER JOIN majors ON majors.major_id = students.major_id WHERE students.user_id=%s"
            cursor = connector.getConnection()
            queryParams = (userId,)
            # if subjectId != "":
            #     sql += " AND subject"
            #     queryParams += (subjectId,)
            cursor.execute(sql,queryParams)
            row = cursor.fetchone()
            if row:
                sql = "SELECT * FROM student_subject_groups ssg " \
                      "INNER JOIN subject_groups sg ON sg.subject_group_id = ssg.subject_group_id " \
                      "WHERE ssg.user_id=%s AND ssg.enable=%s AND sg.enable=%s"
                queryParams = (row['user_id'], "Y", "Y")
                cursor.execute(sql, queryParams)
                row['subjects'] = []
                for record in cursor.fetchall():
                    row['subjects'].append(SubjectService().getSubject(record['subject_id']))
            return row
        except Exception as e:
            print('StudentsService getStudent() Exception[1] ---> ', e)
            print('StudentsService getStudent() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def addStudent(self,studentId,prename,firstname,lastname,email,majorId):
        connector = DBConnector()
        try:
            username = studentId
            password = studentId
            sql = "INSERT INTO users(username, password,prename, firstname, lastname, email) VALUES (%s,%s,%s,%s,%s,%s)"
            queryParams = (username,password,prename,firstname,lastname,email)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            results = {}
            if cursor.rowcount > 0 :
                userId = cursor.lastrowid
                sql = "INSERT INTO students(`user_id`, `student_id`, `major_id`) VALUES(%s,%s,%s)"
                queryParams = (userId,studentId,majorId)
                cursor.execute(sql,queryParams)
                results['user_id'] = userId
                results['student_id'] = studentId
                results['message'] = 'add student success'

            return results
        except Exception as e:
            print(e)
            print('StudentsService addStudent() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def updateStudent(self,userId, studentId,prename,firstname,lastname,email,majorId):
        connector = DBConnector()
        try:
            username = studentId
            password = studentId
            sql = "UPDATE users SET username=%s, password=%s,prename=%s, firstname=%s, lastname=%s, email=%s WHERE user_id=%s"
            queryParams = (username,password,prename,firstname,lastname,email,userId)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            results = {}
            if cursor.rowcount > 0 :
                sql = "UPDATE students SET `major_id`=%s, `subject_id`=%s WHERE user_id=%s"
                queryParams = (studentId,majorId,userId)
                cursor.execute(sql,queryParams)
                results['user_id'] = userId
                results['student_id'] = studentId
                results['message'] = 'update student success'

            return results
        except Exception as e:
            print(e)
            print('StudentsService updateStudent() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def deleteStudent(self,userId):
        connector = DBConnector()
        try:
            sql = "DELETE FROM users WHERE user_id=%s"
            queryParams = (userId)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            results = {}
            results['message'] = 'delete student success'

            return results
        except Exception as e:
            print(e)
            print('StudentsService deleteStudent() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def addSubjectGroup(self,subjectGroupId,userId):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            result = {}
            sql = "INSERT INTO student_subject_groups(subject_group_id,user_id) VALUES(%s,%s)"
            queryParams = (subjectGroupId,userId)
            cursor.execute(sql,queryParams)
            if cursor.rowcount >= 0:
                result['student_subject_group_id'] = cursor.lastrowid
                result['user_id'] = userId
                result['message'] = "add group in subject success"
            return result
        except Exception as e:
            print(e)
            print('StudentsService addSubjectGroup() Exception[2] ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()
