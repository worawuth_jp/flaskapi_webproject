import json

from API.DBAPI import DBConnector
from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from models.responses.ResponseError import ResponseError
from utils.ResponseUtil import ResponseUtil

class SubjectService :
    def getAllSubject(self):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM subjects WHERE status=1"
            cursor = connector.getConnection()
            cursor.execute(sql)
            subjects = []
            for row in cursor.fetchall():
                sql = "SELECT * FROM subject_groups WHERE subject_id =%s "
                queryParams = (row['subject_id'],)
                cursor.execute(sql, queryParams)
                row['subject_groups'] = []
                for subject_group in cursor.fetchall():
                    row['subject_groups'].append(subject_group['subject_group_name'])
                subjects.append(row)

            return subjects
        except Exception as e:
            print('SubjectService getAllSubject() Exception ---> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def getSubject(self,subjectId):
        connector = DBConnector()
        try:
            sql = "SELECT * FROM subjects WHERE subject_id=%s and status=1"
            queryParams = (subjectId,)
            cursor = connector.getConnection()
            cursor.execute(sql,queryParams)
            row = cursor.fetchone()
            sql = "SELECT * FROM subject_groups WHERE subject_id =%s "
            queryParams = (subjectId,)
            cursor.execute(sql, queryParams)
            row['subject_groups'] = []
            for subject_group in cursor.fetchall():
                row['subject_groups'].append(subject_group['subject_group_name'])

            return row
        except Exception as e:
            print('SubjectService getSubject() Exception ---> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def createSubject(self,subjectCode,subjectName,subjectDetail,subjectCourse,subjectTerm):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "INSERT INTO subjects (subject_code,subject_name,subject_detail,subject_term,subject_course) VALUES(%s,%s,%s,%s,%s)"
            subjects = {}
            queryParams = (subjectCode,subjectName,subjectDetail,subjectTerm,subjectCourse)
            cursor.execute(sql,queryParams)
            if cursor.rowcount > 0:
                subjects['subject_id'] = cursor.lastrowid
                subjects['subject_code'] = subjectCode
                subjects['message'] = 'add subject success'

            return subjects
        except Exception as e:
            print('SubjectService addSubject() Exception ---> ',e)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def updateSubject(self,subjectId,subjectCode,subjectName,subjectDetail,subjectCourse,subjectTerm):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "UPDATE subjects SET subject_code=%s,subject_name=%s,subject_detail=%s,subject_course=%s,subject_term=%s WHERE subject_id=%s"
            subjects = {}
            queryParams = (subjectCode,subjectName,subjectDetail,subjectCourse,subjectTerm,subjectId)
            cursor.execute(sql,queryParams)
            if cursor.rowcount >= 0:
                subjects['subject_id'] = subjectId
                subjects['subject_code'] = subjectCode
                subjects['message'] = 'update subject success'

            return subjects
        except Exception as e:
            print('SubjectService updateSubject() Exception ---> ',e)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def updateSubjectStatus(self,subjectId,enable):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "UPDATE subjects SET enable=%s WHERE subject_id=%s"
            subjects = {}
            queryParams = (enable,subjectId)
            cursor.execute(sql,queryParams)
            if cursor.rowcount >= 0:
                subjects['subject_id'] = cursor.lastrowid
                subjects['message'] = 'update subject status'

            return subjects
        except Exception as e:
            print('SubjectService updateSubject() Exception ---> ',e)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def addGroup(self,subjectId,groupName,userId):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "INSERT INTO subject_groups (subject_group_name,subject_id,user_id) VALUES(%s,%s,%s)"
            subjects = {}
            queryParams = (groupName,subjectId,userId)
            cursor.execute(sql,queryParams)
            if cursor.rowcount > 0:
                subjects['subject_group_id'] = cursor.lastrowid
                subjects['subject'] = self.getSubject(subjectId)
                subjects['message'] = 'add subject groups success'

            return subjects
        except Exception as e:
            print('SubjectService addSubject() Exception ---> ',e)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def deleteSubject(self,subjectId):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "DELETE FROM subjects WHERE subject_id=%s"
            subjects = {}
            queryParams = (subjectId,)
            cursor.execute(sql,queryParams)
            if cursor.rowcount >= 0:
                subjects['subject_id'] = subjectId
                subjects['message'] = 'delete subject success'
            return subjects
        except Exception as e:
            print('SubjectService deleteSubject() Exception ---> ',e)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()

    def deleteSubjectGroup(self,subjectGroupId):
        connector = DBConnector()
        cursor = connector.getConnection()
        try:
            sql = "DELETE FROM subject_groups WHERE subject_group_id=%s"
            subjects = {}
            queryParams = (subjectGroupId,)
            cursor.execute(sql,queryParams)
            if cursor.rowcount >= 0:
                subjects['subject_id'] = subjectGroupId
                subjects['message'] = 'delete subject group success'
            return subjects
        except Exception as e:
            print('SubjectService deleteSubjectGroup() Exception ---> ',e)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
        finally:
            print('Close Connection')
            connector.cnx.commit()
            connector.closeConnection()