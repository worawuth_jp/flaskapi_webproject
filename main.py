from os.path import join, dirname

from flask import Flask
from flasgger import Swagger
from flask_cors import CORS
from flask_json_schema import JsonSchema

from models.responses.WelcomeResponseModel import WelcomeResponseModel
from routes.main import router
from dotenv import load_dotenv

from schemas.responses.welcomeSchema import WelcomeSchema

dotenv_path = join(dirname(__file__), '.env')  # Path to .env file
load_dotenv(dotenv_path)

def create_app() :
    schema = JsonSchema()
    app = Flask(__name__)
    CORS(app)
    app.config['SWAGGER'] = {
        'title': 'Flask API Starter Kit',
    }
    swagger = Swagger(app)

    schema.init_app(app)
    router(app)
    return app

if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=5000)
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app = create_app()

    app.run(host='127.0.0.1', port=port,debug=True)

