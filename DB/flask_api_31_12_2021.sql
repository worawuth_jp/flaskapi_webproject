-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2021 at 04:32 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flask_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user_id`) VALUES
(1),
(4);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `custID` char(6) NOT NULL DEFAULT '' COMMENT 'หมายเลขลูกค้า,ตัวเลขรหัส,[LIKE/COUNT/BETWEEN/HAVING],CU-003#CU-004#CU-005#CU-006',
  `custName` varchar(50) DEFAULT NULL COMMENT 'ชื่อลูกค้า,รายละเอียด,[LIKE/UPPER/BETWEEN/LOWER/GROUP BY/ORDER BY],สมศักดิ์#สมศรี#สมหมาย#สมพร',
  `custAddr` varchar(255) DEFAULT NULL COMMENT 'ที่พัก,รายละเอียด,[LIKE/UPPER/BETWEEN/LOWER/GROUP BY/ORDER BY],12 ม.1#555 ม.3#2/11 ม.9#91/2 ม.9',
  `custProvince` varchar(50) DEFAULT NULL COMMENT 'จังหวัด,รายละเอียด,[LIKE/UPPER/BETWEEN/LOWER/GROUP BY/ORDER BY],กรุงเทพ#เชียงใหม่#ชุมพร#ขอนแก่น',
  `custPhone` char(10) DEFAULT NULL COMMENT 'เบอร์ลูกค้า,โทรศัพท์,[COUNT/GROUP BY/ORDER BY],081-2345678#089-8765432#080-0000001#088-1111111'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`custID`, `custName`, `custAddr`, `custProvince`, `custPhone`) VALUES
('CU-001', 'ทับทิม', '214 ม.12', 'ชุมพร', '0864364549'),
('CU-002', 'มรกต', '1/2 ม.1', 'จันทบุรี', '0479623181');

-- --------------------------------------------------------

--
-- Table structure for table `majors`
--

CREATE TABLE `majors` (
  `major_id` int(11) NOT NULL,
  `major_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `majors`
--

INSERT INTO `majors` (`major_id`, `major_name`) VALUES
(1, 'Information Technology');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderNo` char(6) NOT NULL DEFAULT '' COMMENT 'หมายเลขสั่งซื้อ,ตัวเลขรหัส,[LIKE/COUNT/BETWEEN/HAVING],OR-005#OR-006#OR-007#OR-008',
  `orderDate` date DEFAULT NULL COMMENT 'วันที่สั่งซื้อ,เวลา,[GROUP BY/ORDER BY/BETWEEN/COUNT]',
  `orderTotal` int(11) DEFAULT NULL COMMENT 'จำนวนที่สั่งซื้อ, ตัวเลข,[GROUP BY/ORDER BY/AVG/MAX/MIN/SUM/COUNT/STD/HAVING]',
  `custID` char(6) DEFAULT NULL COMMENT 'หมายเลขลูกค้า,ตัวเลขรหัส,[LIKE/COUNT/BETWEEN/HAVING],CU-003#CU-004#CU-005#CU-006'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderNo`, `orderDate`, `orderTotal`, `custID`) VALUES
('OR-002', '2017-10-08', 5, 'CU-001'),
('OR-004', '2017-10-12', 89, 'CU-002');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `prodNo` char(3) NOT NULL DEFAULT '' COMMENT 'หมายเลขสินค้า,ตัวเลขรหัส,[LIKE/COUNT/BETWEEN/HAVING],003#004#005#006',
  `orderNo` char(6) NOT NULL DEFAULT '' COMMENT 'หมายเลขสั่งซื้อ,ตัวเลขรหัส,[LIKE/COUNT/BETWEEN/HAVING],OR-005#OR-006#OR-007#OR-008',
  `quantity` int(11) DEFAULT NULL COMMENT 'จำนวนสั่งซื้อ,ตัวเลข,[GROUP BY/ORDER BY/AVG/MAX/MIN/SUM/COUNT/STD/HAVING]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`prodNo`, `orderNo`, `quantity`) VALUES
('001', 'OR-002', 12),
('002', 'OR-002', 33);

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `PersonID` int(11) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`PersonID`, `LastName`, `FirstName`, `Address`, `City`) VALUES
(1, 'Test', 'Test', 'Test', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prodNo` char(3) NOT NULL DEFAULT '' COMMENT 'หมายเลขสินค้า,ตัวเลขรหัส,[LIKE/COUNT/BETWEEN/HAVING],003#004#005#006',
  `prodName` varchar(50) DEFAULT NULL COMMENT 'ชื่อสินค้า,รายละเอียด,[LIKE/UPPER/BETWEEN/LOWER/GROUP BY/ORDER BY],OPPO#NOKIA#SAMSUNG#APPLE',
  `prodPrice` int(11) DEFAULT NULL COMMENT 'ราคาสินค้า,ตัวเลข,[BETWEEN/GROUP BY/ORDER BY/AVG/MAX/MIN/SUM/COUNT/STD/HAVING]',
  `prodTotal` int(11) DEFAULT NULL COMMENT 'จำนวนสินค้า,ตัวเลข,[BETWEEN/GROUP BY/ORDER BY/AVG/MAX/MIN/SUM/COUNT/STD/HAVING]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prodNo`, `prodName`, `prodPrice`, `prodTotal`) VALUES
('001', 'LUMIA', 61, 51),
('002', 'BARF', 33, 30),
('003', 'SVQTYFDHO', 61, 1);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `question_name` varchar(200) DEFAULT '',
  `question_title` longtext NOT NULL,
  `question_database` varchar(100) NOT NULL,
  `question_num` int(11) NOT NULL,
  `question_type` varchar(10) NOT NULL COMMENT 'S=self,A=Auto',
  `question_end_date` date NOT NULL,
  `question_start_time` time DEFAULT NULL,
  `question_end_time` time NOT NULL,
  `question_status` tinyint(1) NOT NULL DEFAULT 1,
  `question_answer` longtext NOT NULL,
  `question_point` decimal(10,2) NOT NULL,
  `question_standard` int(11) NOT NULL COMMENT 'Only Result = 1\r\nResult And Query = 2',
  `subject_id` int(11) NOT NULL,
  `ref` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `user_id` int(11) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `major_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`user_id`, `student_id`, `major_id`, `subject_id`) VALUES
(42, '74asdf', 1, 1),
(28, 'student001', 1, 1),
(29, 'student1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL,
  `subject_code` varchar(50) NOT NULL,
  `subject_name` varchar(250) NOT NULL,
  `subject_detail` longtext DEFAULT NULL,
  `subject_term` varchar(50) DEFAULT NULL,
  `subject_group` varchar(50) DEFAULT NULL,
  `subject_course` varchar(500) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `subject_code`, `subject_name`, `subject_detail`, `subject_term`, `subject_group`, `subject_course`, `status`) VALUES
(1, 'test001', 'test', 'test', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(255) NOT NULL,
  `tag_name_th` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `user_id` int(11) NOT NULL,
  `teacher_id` varchar(50) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`user_id`, `teacher_id`, `subject_id`) VALUES
(27, 'test_teacher001', 1);

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `template_id` int(11) NOT NULL,
  `template_command` varchar(10) NOT NULL,
  `template_question` longtext NOT NULL,
  `template_answer` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`template_id`, `template_command`, `template_question`, `template_answer`) VALUES
(1, 'UPDATE', '<command> <description> | จาก <data> เป็น <change_data> |ของตาราง <table_name> เป็น <change_data>> |ของตาราง <table_name>  ', 'UPDATE <table_name> SET (<feild_name> = [\"]<change_data> [,]) {1 - n-1}[\"] | [WHERE <feild_name> = [\"]<data>[\"]] '),
(2, 'INSERT', '<command>ข้อมูลใหม่ของ <conjunction>|ลงในตาราง <table_name> <insert_data> | <insert_data>ลงในตาราง <table_name> |สำหรับ <insert_data>ลงในตาราง <table_name> ', 'INSERT INTO <table_name> | <feild_name> | VALUES <insert_data>'),
(3, 'DELETE', '<command> ข้อมูลทั้งหมด <conjunction>ตาราง <table_name> | [ที่มี<feild_name | description> เท่ากับ <data> ]', 'DELETE FROM <table_name> | WHERE (<feild_name> = \"<data> [AND]) {1 - n-1}\" '),
(4, 'DELETE', '<command> <data> ออกจากตาราง <table_name> ', 'DELETE FROM <table_name> | WHERE (<feild_name> = \"<data> [AND]) {1 - n-1}\" '),
(5, 'DELETE', '<command> ข้อมูลทั้งหมดของ <table_name>', 'DELETE FROM <table_name> '),
(6, 'SELECT', '<COMMAND/> & \n[\n	ข้อมูลทั้งหมด\n	|\n	(\n		ตัวพิมพ์ใหญ่ของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n		|\n		ตัวพิมพ์เล็กของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n		|\n		ค่าเฉลี่ยของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n		|\n		ค่าต่ำสุดของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n		|\n		ค่าสูงสุดของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n		|\n		จำนวนข้อมูลทั้งหมด[ของ <FILED_NAME/>] & [โดยใช้ชื่อ <NAME/>]\n		|\n		ผลรวมของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n		|\n		<FILED_NAME/>{n} & [โดยใช้ชื่อ <NAME/>]\n		|\n		DISTINCT ของ <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n	)\n] & \nจากตาราง <TABLE_NAME/> \n++\n(\n	[\n		ที่มีค่าของ & <FILED_NAME/> & \n		เท่ากับ <VALUE/>|มากกว่า <VALUE/>|น้อยกว่า <VALUE/>|\n		ไม่เท่ากับ <VALUE/>|มากกว่าเท่ากับ <VALUE/>|\n		น้อยกว่าเท่ากับ <VALUE/>|ซึ่งเป็นคำที่มีค่า \"<VALUE/>\"ประกอบ|\n		ซึ่งอยู่ระหว่าง \"<VALUE/> - <VALUE2/>\"|\n		อยู่ใน <LISTS/>\n	]\n	|\n	[\n		(จัดเรียงตาม & <FILED_NAME/>{n})\n		| \n		[\n			จัดเรียงตาม & <FILED_NAME/>{n} & ที่มี &\n			ค่าเฉลี่ยของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n			|\n			ค่าต่ำสุดของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n			|\n			ค่าสูงสุดของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n			|\n			จำนวนข้อมูลทั้งหมด[ของ <FILED_NAME/>] & [โดยใช้ชื่อ <NAME/>]\n			|\n			ผลรวมของ & <FILED_NAME/> & [โดยใช้ชื่อ <NAME/>]\n			|\n			<FILED_NAME/>{n} & [โดยใช้ชื่อ <NAME/>]\n		]\n		\n	] \n	&\n	[\n		ที่มีค่าของ & <FILED_NAME/> & \n		เท่ากับ <VALUE/>|มากกว่า <VALUE/>|น้อยกว่า <VALUE/>|\n		ไม่เท่ากับ <VALUE/>|มากกว่าเท่ากับ <VALUE/>|\n		น้อยกว่าเท่ากับ <VALUE/>|ซึ่งเป็นคำที่มีค่า \"<VALUE/>\"ประกอบ|\n		ซึ่งอยู่ระหว่าง \"<VALUE/> - <VALUE2/>\"\n	]\n	|\n	[จัดเรียงตาม & <FILED_NAME/> & จาก[น้อยไปมาก | มากไปน้อย]]\n)', '<SELECT [remember|understand|implement|analysis]>\n	<COLUMN>\n		*\n		|\n		<OPTION>\n			<UPPER>\n				<FILED_NAME/>\n				[<NAME/>]\n			</UPPER>\n			|\n			<LOWER>\n				<FILED_NAME/>\n				[<NAME/>]\n			</LOWER>\n			|\n			<AVG>\n				<FILED_NAME/>\n				[<NAME/>]\n			</AVG>\n			|\n			<MIN>\n				<FILED_NAME/>\n				[<NAME/>]\n			</MIN>\n			|\n			<MAX>\n				<FILED_NAME/>\n				[<NAME/>]\n			</MAX>\n			|\n			<COUNT>\n				*|<FILED_NAME/>\n				[<NAME/>]\n			</COUNT>\n			|\n			<SUM>\n				<FILED_NAME/>\n				[<NAME/>]\n			</SUM>\n			|\n			<FILED_NAME N=\"n\">\n				<VALUE/>\n				[<NAME/>]\n			</FILED_NAME>\n			|\n			<DISTINCT>\n				<FILED_NAME/>\n				[<NAME/>]\n			</DISTINCT>\n		</OPTION>\n	</COLUMN>\n	<TABLE_NAME/>\n	[<OPTION [SEQ]>\n		<WHERE NumCon=\"n\">\n			<CON N=\"n\">\n				[<PRE>AND|OR</PRE>]\n				<FILED_NAME/>\n				<OPERATOR>=|>|<|<>|>=|<=|LIKE|BETWEEN|IN</OPERATOR>\n				<VALUE/>\n				[<VALUE2/>]\n				[<VALUES/>]\n			</CON>\n		</WHERE>\n		|\n		(\n			<GROUPBY>\n				<FILED_NAME N=\"n\"/>\n			</GROUPBY>\n			|\n			<HAVING>\n				<CON N=\"n\">\n					[<PRE>AND|OR</PRE>]\n					<FILED_NAME/>|[<AVG/>|<MIN/>|<MAX/>|<COUNT/>|<SUM/>]\n					<OPERATOR>=|<|<>|>|>=|<=|LIKE|BETWEEN</OPERATOR>\n					<VALUE/>\n					[<VALUE2/>]\n				</CON>\n			<HAVING/>\n		)\n		|\n		<ORDERBY>\n			<FILED_NAME N=\"n\"/>\n			<VALUE>ASC|DESC</VALUE>\n		</ORDERBY>\n	</OPTION>]\n</SELECT>');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `template_id` int(11) NOT NULL,
  `template_database` varchar(200) NOT NULL DEFAULT '',
  `template_quest` longtext NOT NULL,
  `template_answer` longtext NOT NULL,
  `template_sql_type` varchar(50) NOT NULL,
  `template_skill_type` varchar(50) NOT NULL COMMENT 'R=ความจำ,U=ความเข้าใจ,I=การประยุกต์,A=วิเคระห์'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `prename` varchar(20) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `prename`, `firstname`, `lastname`, `email`) VALUES
(1, 'admin', 'password', NULL, 'admin', 'test', 'admin_test@gmail.com'),
(4, 'test_insert2', 'test_insert2', 'นาย', 'test_firstname', 'test_lastname', 'test_insert2@example.com'),
(27, 'test_teacher', 'test_teacher', 'นาย', 'test_teacher', 'test_teacher', 'test_teacher@example.com'),
(28, 'test_student', 'test_student', 'นาย', 'test_student', 'test_student', 'test_student@example.com'),
(29, 'student1', 'student1', 'prename', 'student_name_test_edit', 'student_name', 'student1@example.com'),
(42, '74asdf', '74asdf', 'นาย', 'Worawuth', 'Puntusithsaree', 'jay.worawuth@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`custID`);

--
-- Indexes for table `majors`
--
ALTER TABLE `majors`
  ADD PRIMARY KEY (`major_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderNo`),
  ADD KEY `Foreign Key` (`custID`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`orderNo`,`prodNo`),
  ADD KEY `Foreign Key` (`prodNo`),
  ADD KEY `Foreign Key2` (`orderNo`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prodNo`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD UNIQUE KEY `student_id` (`student_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `major_id` (`major_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`),
  ADD KEY `subject_code` (`subject_code`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD UNIQUE KEY `teacher_id` (`teacher_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`template_id`),
  ADD KEY `tempale_answer` (`template_answer`(768)),
  ADD KEY `template_sql_type` (`template_sql_type`),
  ADD KEY `template_skill_type` (`template_skill_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `majors`
--
ALTER TABLE `majors`
  MODIFY `major_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `customer_details_ibfk_1` FOREIGN KEY (`custID`) REFERENCES `customer` (`custID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `orders_details_ibfk_1` FOREIGN KEY (`orderNo`) REFERENCES `orders` (`orderNo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_details_ibfk_1` FOREIGN KEY (`prodNo`) REFERENCES `product` (`prodNo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `students_ibfk_2` FOREIGN KEY (`major_id`) REFERENCES `majors` (`major_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `students_ibfk_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teachers_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
