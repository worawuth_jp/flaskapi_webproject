import json
from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from

from controllers.SubjectController import SubjectController
from schemas.responses.Response import Response

subject_api = Blueprint('subject', __name__)

@subject_api.route('/subjects',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def listSubject():
    try:
        params = request.args.to_dict()
        if len(params) > 0:
            result = SubjectController().listSubject(params)
        else:
            result = SubjectController().listSubjects()
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('SUBJECT Route Exception ----> ',e)
        return Response().dump(e), e.code


@subject_api.route('/subject',methods=['POST'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def createSubject():
    try:
        body = request.get_json()
        result = SubjectController().createSubject(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('SUBJECT Route Exception ----> ',e)
        return Response().dump(e), e.code

@subject_api.route('/subject',methods=['PUT'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def updateSubject():
    try:
        body = request.get_json()
        result = SubjectController().updateSubject(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('SUBJECT Route Exception ----> ',e)
        return Response().dump(e), e.code

@subject_api.route('/subject/status',methods=['PUT'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def updateSubjectStatus():
    try:
        body = request.get_json()
        result = SubjectController().updateSubjectStatus(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('SUBJECT Route Exception ----> ',e)
        return Response().dump(e), e.code

@subject_api.route('/subject/group',methods=['POST'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def addGroupToSubject():
    try:
        body = request.get_json()
        result = SubjectController().addGroup(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('SUBJECT Route Exception ----> ',e)
        return Response().dump(e), e.code

@subject_api.route('/subject',methods=['DELETE'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def deleteSubject():
    try:
        body = request.args.to_dict()
        result = SubjectController().deleteSubject(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('SUBJECT Route Exception ----> ',e)
        return Response().dump(e), e.code
