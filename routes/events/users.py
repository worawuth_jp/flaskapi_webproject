from http import HTTPStatus

import json
from flask import Blueprint, jsonify, request
from flasgger import swag_from
from controllers.UserController import UserController
from schemas.responses.Response import Response, ResponseWithData

users_api = Blueprint('users', __name__)

@users_api.route('/users',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def listUsers():
    try:
        result = UserController().listUserData()
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('User Route ---> listUsers() Exception ----> ',e)
        return Response().dump(e), e.code

@users_api.route('/user/login',methods=['POST'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Login',
            'schema': Response
        }
    }
})
def login():
    try:
        req = request.get_json()
        # print(f'Request Data [%s] ----> %s'%(len(req),json.dumps(req)))
        result = UserController().loginUser(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('User Route ---> login() Exception ----> ',e)
        return Response().dump(e), e.code

@users_api.route('/user/generate',methods=['POST'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Login',
            'schema': Response
        }
    }
})
def userGenerate():
    try:
        req = request.get_json()
        print(f'Request Data [%s] ----> %s'%(len(req),json.dumps(req)))
        result = UserController().userGenerate(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('User Route ---> userGenerate() Exception ----> ',e)
        return Response().dump(e), e.code