import json
from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from

from controllers.AdminController import AdminController
from controllers.QuestionController import QuestionController
from schemas.responses.Response import Response

question_api = Blueprint('question', __name__)

@question_api.route('/questions',methods=['GET'])
def listQuestion():
    try:
        req = request.args.to_dict()
        result = QuestionController().listQuestions(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('listQuestion listQuestion Route Exception ----> ',e)
        return Response().dump(e), e.code

@question_api.route('/questions/search',methods=['GET'])
def searchQuestion():
    try:
        req = request.args.to_dict()
        result = QuestionController().searchQuestion(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('listQuestion searchQuestion Route Exception ----> ',e)
        return Response().dump(e), e.code

@question_api.route('/question',methods=['POST'])
def addQuestion():
    try:
        req = request.get_json()
        print(f'Request Data [%s] ----> %s' % (len(req), json.dumps(req)))
        result = QuestionController().addQuestion(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Question Route Exception ----> ',e)
        return jsonify(e.__dict__), e.code

@question_api.route('/question',methods=['PUT'])
def updateQuestion():
    try:
        req = request.get_json()
        print(f'Request Data [%s] ----> %s' % (len(req), json.dumps(req)))
        result = QuestionController().updateQuestion(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Question Route Exception ----> ',e)
        return jsonify(e.__dict__), e.code

@question_api.route('/question',methods=['DELETE'])
def deleteAdmin():
    try:
        req = request.args.to_dict()
        print(f'Request Data [%s] ----> %s' % (len(req), json.dumps(req)))
        result = QuestionController().deleteQuestion(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Question Route Exception ----> ',e)
        return jsonify(e.__dict__), e.code