from http import HTTPStatus
from flask import Blueprint
from flasgger import swag_from
from controllers.UserController import UserController
from schemas.responses.Response import Response, ResponseWithData
from schemas.responses.welcomeSchema import WelcomeSchema
from models.responses.WelcomeResponseModel import WelcomeResponseModel

home_api = Blueprint('api', __name__)


@home_api.route('/',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Welcome to the Flask API',
            'schema': WelcomeSchema
        }
    }
})
def welcome():
    """
    1 liner about the route
    A more detailed description of the endpoint
    ---
    """
    result = WelcomeResponseModel("Welcome")
    return WelcomeSchema().dump(result), 200
