import json
from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from

from controllers.AdminController import AdminController
from schemas.responses.Response import Response

admin_api = Blueprint('admin', __name__)

@admin_api.route('/admin',methods=['GET'])
def listAdmin():
    try:
        params = request.args.to_dict()
        if len(params) > 0:
            result = AdminController().listAdmin(params)
        else:
            result = AdminController().listAdmin(params)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Admin Route Exception ----> ',e)
        return Response().dump(e), e.code

@admin_api.route('/admin',methods=['POST'])
def addAdmin():
    try:
        req = request.get_json()
        print(f'Request Data [%s] ----> %s' % (len(req), json.dumps(req)))
        result = AdminController().addAdmin(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Admin Route Exception ----> ',e)
        return jsonify(e.__dict__), e.code

@admin_api.route('/admin',methods=['PUT'])
def updateAdmin():
    try:
        req = request.get_json()
        print(f'Request Data [%s] ----> %s' % (len(req), json.dumps(req)))
        result = AdminController().updateAdmin(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Admin Route Exception ----> ',e)
        return jsonify(e.__dict__), e.code

@admin_api.route('/admin',methods=['DELETE'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def deleteAdmin():
    try:
        req = request.args.to_dict()
        print(f'Request Data [%s] ----> %s' % (len(req), json.dumps(req)))
        result = AdminController().deleteAdmin(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Admin Route Exception ----> ',e)
        return jsonify(e.__dict__), e.code