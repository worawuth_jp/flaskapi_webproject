import json
from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from

from controllers.TeachersController import TeachersController
from schemas.responses.Response import Response

teachers_api = Blueprint('teachers', __name__)

@teachers_api.route('/teachers',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def listTeachers():
    try:
        params = request.args.to_dict()
        if len(params) > 0:
            result = TeachersController().listTeacherById(params)
        else:
            result = TeachersController().listTeachers()
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Teachers Route Exception ----> ',e)
        return Response().dump(e), e.code

@teachers_api.route('/teacher/search',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def searchTeachers():
    try:
        params = request.args.to_dict()
        result = TeachersController().searchTeacher(params)

        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Teachers Route Exception ----> ',e)
        return Response().dump(e), e.code

@teachers_api.route('/teacher',methods=['POST'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def createTeachers():
    try:
        body = request.get_json()
        result = TeachersController().addTeacher(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Teachers Route Exception ----> ',e)
        return Response().dump(e), e.code

@teachers_api.route('/teacher',methods=['PUT'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def updateTeacher():
    try:
        body = request.get_json()
        result = TeachersController().updateTeacher(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Teachers Route Exception ----> ',e)
        return Response().dump(e), e.code

@teachers_api.route('/teacher',methods=['DELETE'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def deleteTeacher():
    try:
        req = request.args.to_dict()
        result = TeachersController().deleteTeacher(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Teachers Route Exception ----> ',e)
        return Response().dump(e), e.code