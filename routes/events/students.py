import json
from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from

from controllers.StudentsController import StudentsController
from schemas.responses.Response import Response

students_api = Blueprint('students', __name__)

@students_api.route('/majors',methods=['GET'])
def listMajors():
    try:
        result = StudentsController().listMajor()
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Student Route listMajors() Exception ----> ',e)
        return Response().dump(e), e.code

@students_api.route('/students',methods=['GET'])
def listStudents():
    try:
        result = StudentsController().listStudents()
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Student Route listStudents() Exception ----> ',e)
        return Response().dump(e), e.code

@students_api.route('/student',methods=['GET'])
def listStudent():
    try:

        req = request.args.to_dict()
        print("Request Params ----> ", request.args.to_dict())
        result = StudentsController().listStudent(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Student Route listStudents() Exception ----> ',e)
        return Response().dump(e), e.code

@students_api.route('/student',methods=['POST'])

def addStudent():
    try:
        req = request.get_json()
        print("Request ---> ",req)
        result = StudentsController().addStudent(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Student Route addStudent() Exception ----> ',e)
        return Response().dump(e), e.code

@students_api.route('/student/group',methods=['POST'])

def addGroup():
    try:
        req = request.get_json()
        print("Request ---> ",req)
        result = StudentsController().addGroup(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Student Route addStudent() Exception ----> ',e)
        return Response().dump(e), e.code

@students_api.route('/student',methods=['PUT'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def updateStudent():
    try:
        req = request.get_json()
        result = StudentsController().updateStudent(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Student Route updateStudent Exception ----> ',e)
        return Response().dump(e), e.code