import json
from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from

from controllers.DatabaseController import DatabaseController
from schemas.responses.Response import Response

database_api = Blueprint('database', __name__)

@database_api.route('/databases',methods=['GET'])
def listDatabase():
    try:
        result = DatabaseController().listDatabase()
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Database Route Exception ----> ',e)
        return Response().dump(e), e.code

@database_api.route('/tables',methods=['GET'])
def listTable():
    try:
        req = request.args.to_dict()
        result = DatabaseController().listTable(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Database Route Exception ----> ',e)
        return Response().dump(e), e.code

@database_api.route('/query',methods=['POST'])
def queryDatabase():
    try:
        body = request.get_json()
        result = DatabaseController().queryDatabase(body)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Database Route Exception ----> ',e)
        return Response().dump(e), e.code
