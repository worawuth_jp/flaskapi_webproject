import json
from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from

from controllers.TemplateController import TemplateController
from schemas.responses.Response import Response

template_api = Blueprint('template', __name__)

@template_api.route('/templates',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Get Users',
            'schema': Response
        }
    }
})
def listTemplate():
    try:
        result = TemplateController().listTemplate()
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Template Route Exception ----> ',e)
        return Response().dump(e), e.code

@template_api.route('/template',methods=['POST'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Create Template',
            'schema': Response
        }
    }
})
def addTemplate():
    try:
        req = request.get_json()
        result = TemplateController().addTemplate(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Template Route addTemplate() Exception ----> ',e)
        return Response().dump(e), e.code

@template_api.route('/template',methods=['PUT'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Create Template',
            'schema': Response
        }
    }
})
def updateTemplate():
    try:
        req = request.get_json()
        result = TemplateController().updateTemplate(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Template Route addTemplate() Exception ----> ',e)
        return Response().dump(e), e.code

@template_api.route('/template/search',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Create Template',
            'schema': Response
        }
    }
})
def searchTemplate():
    try:
        req = request.args.to_dict()
        print('Request Params ---> ',req)
        result = TemplateController().searchTemplate(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Template Route addTemplate() Exception ----> ',e)
        return Response().dump(e), e.code

@template_api.route('/template/select',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Create Template',
            'schema': Response
        }
    }
})
def selectSearchTemplate():
    try:
        req = request.args.to_dict()
        print('Request Params ---> ',req)
        result = TemplateController().selectTemplate(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Template Route selectSearchTemplate() Exception ----> ',e)
        return Response().dump(e), e.code

@template_api.route('/template/gen',methods=['GET'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Create Template',
            'schema': Response
        }
    }
})
def generateTemplate():
    try:
        req = request.args.to_dict()
        print('Request Params ---> ',req)
        result = TemplateController().genTemplate(req)
        return jsonify(result.__dict__), 200
    except Exception as e:
        print('Template Route generateTemplate() Exception ----> ',e)
        return Response().dump(e), e.code