from http import HTTPStatus
from flask import Blueprint

from models.responses.WelcomeResponseModel import WelcomeResponseModel
from schemas.responses.welcomeSchema import WelcomeSchema


error_api = Blueprint('errors', __name__)

@error_api.errorhandler(404)
def welcome():
    result = WelcomeResponseModel("404 Not Found")
    return WelcomeSchema().dump(result), 404