from models.responses.WelcomeResponseModel import WelcomeResponseModel
from routes.events.admin import admin_api
from routes.events.database import database_api
from routes.events.home import home_api
from routes.events.question import question_api
from routes.events.students import students_api
from routes.events.subjects import subject_api
from routes.events.teachers import teachers_api
from routes.events.users import users_api
from routes.events.template import template_api
from schemas.responses.welcomeSchema import WelcomeSchema


def router(app) :
    @app.errorhandler(404)
    def handle_page_not_found(e):
        result = WelcomeResponseModel("404 Not Found")
        return WelcomeSchema().dump(result), 404

    app.register_error_handler(404,handle_page_not_found)
    app.register_blueprint(home_api, url_prefix='/api')
    app.register_blueprint(users_api, url_prefix='/api')
    app.register_blueprint(teachers_api, url_prefix='/api')
    app.register_blueprint(students_api, url_prefix='/api')
    app.register_blueprint(admin_api, url_prefix='/api')
    app.register_blueprint(template_api, url_prefix='/api')
    app.register_blueprint(subject_api, url_prefix='/api')
    app.register_blueprint(database_api, url_prefix='/api')
    app.register_blueprint(question_api, url_prefix='/api')
