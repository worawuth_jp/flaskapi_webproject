ERROR_INSERT_TEMPLATE_MSG = "เพิ่ม Template ไม่สำเร็จ"
ERROR_UPDATE_TEMPLATE_MSG = "แก้ไข Template ไม่สำเร็จ"
ERROR_DELETE_TEMPLATE_MSG = "ลบ Template ไม่สำเร็จ"
SKILL_READ_TYPE = "R"
SKILL_UNDERSTAND_TYPE = "U"
SKILL_IMPLEMENT_TYPE = "I"
SKILL_ANALYSIS_TYPE = "A"
TABLES = ["customer","orders","order_details","product"]
# {"flask_api":["customer","orders","order_details","product"],"new_database":["t1","t2]}

DATABASE = ["flask_api"] #Fetch Data from db

productNameSet = ["LUMI2","Prod","BARFT","TRATOR"]
custNameSet = ["สมศักดิ์","สมศรี","สมปอง","สมหมาย","สมปอง","สมพร"]
custAddrSet = ["11/2","12/2","12/1","13/4","108/9"]
custProvinceSet = ["กรุงเทพ","เชียงใหม่","เชียงราย","น่าน","นครปฐม","กาญจนบุรี"]
custPhoneSet = ["088-8881234","02-541236","090-0900000","092-2221111","087-77777777","062-6556555","098-8989988"]
