TYPE_ADMIN = 'ADMIN'
TYPE_TEACHER = 'TEACHER'
TYPE_STUDENT = 'STUDENT'

ERROR_GENERATE_MSG = 'ไม่สามารถสร้างผุ้ใช้งานใหม่ได้'
ERROR_USERNAME_UNIQUE_MSG = 'ไม่สามารถใช้ username นี้ได้ เนื่องจากมี username นี้ในระบบแล้ว'