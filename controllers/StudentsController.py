import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from models.responses.ResponseError import ResponseError
from services.StudentsService import StudentsService
from utils.ResponseUtil import ResponseUtil


class StudentsController:
    def listStudents(self):
        try:
            resultSuccess = StudentsService().getListsStudents()
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('StudentsController listStudents() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def listMajor(self):
        try:
            resultSuccess = StudentsService().listMajor()
            return resultSuccess
        except Exception as e:
            print('StudentsController listMajor() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def listStudent(self,req):
        try:
            if 'user_id' not in req  :
                print('StudentsController listStudent() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if req['user_id'] == '':
                print('StudentsController listStudent() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            subjectId = ""
            if 'subject_id' in req:
                subjectId = req['subject_id']
            print("Prepare.......")
            userId = req['user_id']
            print("Prepare------>2.......")
            resultSuccess = StudentsService().getStudent(userId,subjectId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('StudentsController listStudent() Exception[1] ----> ',e.code)
            print('StudentsController listStudent() Exception[2] ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            if e.code == HTTP_BAD_REQUEST_CODE:
                raise ResponseError(e.code, e.msg)
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def addStudent(self, body):
        try:
            if 'student_id' not in body or 'firstname' not in body or 'lastname' not in body or 'email' not in body or 'major_id' not in body or 'subject_id' not in body:
                print('StudentsController addStudent() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['student_id'] == '' or body['firstname'] == '' or body['lastname'] == '' or body['email'] == '' or body['major_id'] == '' or body['subject_id'] == '' :
                print('StudentsController addStudent() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            studentId = body['student_id']
            firstname = body['firstname']
            lastname = body['lastname']
            email = body['email']
            prename = body['prename']
            majorId = body['major_id']
            subjectId = body['subject_id']

            resultSuccess = StudentsService().addStudent(studentId,prename,firstname,lastname,email,majorId,subjectId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('StudentsController addStudent() Exception ----> ',
                  json.dumps(e))
            raise ResponseError(e.code, e.msg)

    def addGroup(self, body):
        try:
            if 'subject_group_id' not in body or 'user_id' not in body:
                print('StudentsController addGroup() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['subject_group_id'] == '' or body['user_id'] == '' :
                print('StudentsController addGroup() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            subjectGroupId = body['subject_group_id']
            userId = body['user_id']

            resultSuccess = StudentsService().addSubjectGroup(subjectGroupId,userId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('StudentsController addGroup() Exception ----> ',
                  json.dumps(e))
            raise ResponseError(e.code, e.msg)

    def updateStudent(self, body):
        try:
            if 'student_id' not in body or 'firstname' not in body or 'lastname' not in body or 'email' not in body or 'major_id' not in body or 'subject_id' not in body:
                print('StudentsController addStudent() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['student_id'] == '' or body['firstname'] == '' or body['lastname'] == '' or body['email'] == '' or body['major_id'] == '' or body['subject_id'] == '' :
                print('StudentsController addStudent() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            studentId = body['student_id']
            prename = body['prename']
            firstname = body['firstname']
            lastname = body['lastname']
            email = body['email']
            majorId = body['major_id']
            subjectId = body['subject_id']
            userId = body['user_id']

            resultSuccess = StudentsService().updateStudent(userId,studentId,prename,firstname,lastname,email,majorId,subjectId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('StudentsController addStudent() Exception ----> ',
                  json.dumps(e))
            raise ResponseError(e.code, e.msg)
