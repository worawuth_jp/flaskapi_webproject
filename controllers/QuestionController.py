import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from models.responses.ResponseError import ResponseError
from services.AdminService import AdminService
from services.QuestionService import QuestionService


class QuestionController:
    def listQuestions(self,req):
        try:
            isBodyUndefinedOrEmpty = True if not (len(req) > 0 or req) else False
            subject_id = ""
            user_id = ""
            if not isBodyUndefinedOrEmpty :
                if ('subject_id' in req):
                    subject_id = req['subject_id']
                if ('user_id' in req):
                    user_id = req['user_id']

            resultSuccess = QuestionService().getAllQuestions(subject_id,user_id)
            return resultSuccess
        except Exception as e:
            print('QuestionController listQuestions() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)

    def searchQuestion(self,req):
        try:
            isBodyUndefinedOrEmpty = True if not (len(req) > 0 or req) else False
            text = ""
            if not isBodyUndefinedOrEmpty:
                if ('text' in req):
                    text = req['text']

            resultSuccess = QuestionService().searchQuestion(text)
            return resultSuccess
        except Exception as e:
            print('QuestionController searchQuestion() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)

    def addQuestion(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('question_answer' not in body or 'question_database' not in body or 'question_end_date' not in body
                                          or "question_end_time" not in body or 'question_num' not in body or "question_point" not in body or "question_status" not in body
                or "question_standard" not in body  or "question_title" not in body  or "question_type" not in body  or "subject_id" not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)


            answer = body['question_answer']
            database = body['question_database']
            endDate = body['question_end_date']
            startTime = body['question_start_time']
            endTime = body['question_end_time']
            num = body['question_num']
            questPoint = body['question_point']
            standard = body['question_standard']
            title = body['question_title']
            name = body['question_name']
            type = body['question_type']
            skillType = body['question_skill_type']
            sqlType = body['question_sql_type']
            subjectId = body['subject_id']
            status = body['question_status']

            resultSuccess = QuestionService().addQuestion(name,title,database,num,type,endDate,startTime,endTime,answer,questPoint,standard,subjectId,skillType,sqlType,status)
            return resultSuccess
        except Exception as e:
            print('QuestionController addQuestion() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def updateQuestion(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or (
                    'question_answer' not in body or 'question_database' not in body or 'question_end_date' not in body
                    or "question_end_time" not in body or 'question_num' not in body or "question_point" not in body
                    or "question_standard" not in body or "question_title" not in body or "question_type" not in body or "subject_id" not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            answer = body['question_answer']
            database = body['question_database']
            endDate = body['question_end_date']
            startTime = body['question_start_time']
            endTime = body['question_end_time']
            num = body['question_num']
            questPoint = body['question_point']
            standard = body['question_standard']
            title = body['question_title']
            name = body['question_name']
            type = body['question_type']
            subjectId = body['subject_id']
            questionId = body['question_id']
            resultSuccess = QuestionService().updateQuestion(questionId,name,title, database, num, type, endDate, startTime, endTime, answer,
                                                          questPoint, standard, subjectId)
            return resultSuccess
        except Exception as e:
            print('QuestionController updateQuestion() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def deleteQuestion(self, body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ( 'question_id' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['question_id'] == '' :
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            questionId = body['question_id']
            resultSuccess = QuestionService().deleteQuestion(questionId)
            return resultSuccess
        except Exception as e:
            print('QuestionService deleteQuestion() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)