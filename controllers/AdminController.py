import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from models.responses.ResponseError import ResponseError
from services.AdminService import AdminService
from utils.ResponseUtil import ResponseUtil


class AdminController:
    def listAdmin(self,params):
        try:
            resultSuccess = AdminService().getAllAdmin()
            if 'user_id' in params:
                resultSuccess = AdminService().getAdminByUserId(params['user_id'])
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('AdminController listAdmin() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)

    def addAdmin(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('username' not in body or 'password' not in body or 'email' not in body or "firstname" not in body or 'lastname' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)
            if body['username'] == '' or body['password'] == '' or body['email'] == '' or body['firstname'] == '' or body['lastname'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            username = body['username']
            password = body['password']
            prename = body['prename']
            firstname = body['firstname']
            lastname = body['lastname']
            email = body['email']
            resultSuccess = AdminService().addAdmin(username,password,prename,firstname,lastname,email)
            return resultSuccess
        except Exception as e:
            print('AdminController addAdmin() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def updateAdmin(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('user_id' not in body or 'username' not in body or 'password' not in body or 'email' not in body or 'prename' not in body or "firstname" not in body or 'lastname' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)
            if body['user_id'] == '' or body['username'] == '' or body['password'] == '' or body['email'] == '' or body['prename'] == '' or body['firstname'] == '' or body['lastname'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            userId = body['user_id']
            username = body['username']
            password = body['password']
            prename = body['prename']
            firstname = body['firstname']
            lastname = body['lastname']
            email = body['email']
            resultSuccess = AdminService().updateAdmin(userId,username,password,prename,firstname,lastname,email)
            return resultSuccess
        except Exception as e:
            print('AdminController updateAdmin() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def deleteAdmin(self, body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ( 'user_id' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['user_id'] == '' :
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            userId = body['user_id']
            resultSuccess = AdminService().deleteAdmin(userId)
            return resultSuccess
        except Exception as e:
            print('AdminController deleteAdmin() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)