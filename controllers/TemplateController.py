import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from constants.template import SKILL_READ_TYPE,SKILL_UNDERSTAND_TYPE,SKILL_ANALYSIS_TYPE,SKILL_IMPLEMENT_TYPE
from models.responses.ResponseError import ResponseError
from services.TemplateService import TemplateService
from utils.ResponseUtil import ResponseUtil


class TemplateController:
    def listTemplate(self):
        try:
            resultSuccess = TemplateService().getAllTemplate()
            return resultSuccess
        except Exception as e:
            print('TemplateController listTemplate() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)

    def genTemplate(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or (
                    'cmd' not in body or 'database' not in body or 'table_name' not in body or 'skill_type' not in body ):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['cmd'] == '' or body['database'] == '' or body['table_name'] == '' or body['skill_type'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            cmd = body['cmd']
            database = body['database']
            tableName = body['table_name']
            skillType = body['skill_type']
            resultSuccess = TemplateService().generateSelectQuestion(cmd,skillType,database,tableName)
            resultSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            return resultSuccess
        except Exception as e:
            print('TemplateController genTemplate() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)

    def addTemplate(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('template_question' not in body or 'template_answer' not in body or 'template_skill_type' not in body or "template_sql_type" not in body ):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)
            if body['template_question'] == '' or body['template_answer'] == '' or body['template_skill_type'] == '' or body['template_sql_type'] == '' :
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            question = body['template_question']
            answer = body['template_answer']
            database = body['template_database']
            skillType = ""
            for skill in body['template_skill_type'] :
                skill += skillType.upper()

            sqlType = body['template_sql_type']
            resultSuccess = TemplateService().createTemplate(database,question,answer,skillType,sqlType)
            return resultSuccess
        except Exception as e:
            print('TemplateController addTemplate() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def updateTemplate(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('template_question' not in body or 'template_id' not in body or 'template_answer' not in body or 'template_skill_type' not in body or "template_sql_type" not in body ):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)
            if body['template_question'] == '' or body['template_id'] == '' or body['template_answer'] == '' or body['template_skill_type'] == '' or body['template_sql_type'] == '' :
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            tempId = body['template_id']
            question = body['template_question']
            answer = body['template_answer']
            database = body['template_database']
            skillType = ""
            for skill in body['template_skill_type'] :
                print(skill)
                skill += skillType.upper()

            sqlType = body['template_sql_type']
            resultSuccess = TemplateService().updateTemplate(tempId,database,question,answer,skillType,sqlType)
            return resultSuccess
        except Exception as e:
            print('TemplateController updateTemplate() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def deleteTemplate(self,params):
        try:
            isBodyUndefinedOrEmpty = True if not (len(params) > 0 or params) else False
            if isBodyUndefinedOrEmpty or ('template_id' not in params ):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)
            if params['template_id'] == '' :
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            tempId = params['template_id']
            resultSuccess = TemplateService().deleteTemplate(tempId)
            return resultSuccess
        except Exception as e:
            print('TemplateController deleteTemplate() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def searchTemplate(self,params):
        try:
            isBodyUndefinedOrEmpty = True if not (len(params) > 0 or params) else False
            if isBodyUndefinedOrEmpty or ('template_skill_type' not in params and 'template_sql_type' not in params ):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)

            skillType = ""
            sqlType = ""
            if 'template_skill_type' in params :
                skillType = params['template_skill_type']
            if 'template_sql_type' in params:
                sqlType = params['template_sql_type']
            resultSuccess = TemplateService().searchByType(skillType,sqlType)
            return resultSuccess
        except Exception as e:
            print('TemplateController searchTemplate() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)

    def selectTemplate(self,params):
        try:
            isBodyUndefinedOrEmpty = True if not (len(params) > 0 or params) else False
            if isBodyUndefinedOrEmpty or ('template_skill_type' not in params and 'template_sql_type' not in params ):
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)

            skillType = ""
            sqlType = ""
            if 'template_skill_type' in params:
                skillType = params['template_skill_type']
            if 'template_sql_type' in params:
                sqlType = params['template_sql_type']
            resultSuccess = TemplateService().selectSearchByType(skillType,sqlType)
            return ResponseUtil().transformResponseWithData(resultSuccess)
        except Exception as e:
            print('TemplateController selectTemplate() Exception ----> ', json.dumps(e.__dict__))
            raise ResponseError(e.code, e.msg)