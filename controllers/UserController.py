import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from constants.users import TYPE_ADMIN, TYPE_TEACHER, TYPE_STUDENT
from models.responses.ResponseError import ResponseError
from services.UserServices import UserServices


class UserController :
    def listUserData(self):
        try:
            resultSuccess = UserServices().getAllUsers()
            return resultSuccess
        except Exception as e:
            print('UserController listUserData() Exception ----> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)

    def loginUser(self,body):
        try:
            if len(body) == 0 or 'password' not in body or 'username' not in body or 'type_user' not in body :
                print('validation request error')
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)
            if len(body['username']) == 0 or len(body['password']) == 0 or len(body['type_user']) == 0 or (body['type_user'].upper() != TYPE_ADMIN and body['type_user'].upper() != TYPE_TEACHER and body['type_user'].upper() != TYPE_STUDENT):
                print('validation request empty or invalid')
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            username = body['username']
            password = body['password']
            typeUser = body['type_user']
            resultSuccess = UserServices().loginUser(typeUser=typeUser,username=username,password=password)
            print('resultSuccess ---> ',json.dumps(resultSuccess.__dict__))
            return resultSuccess
        except Exception as e:

            print('UserController loginUser() Exception ----> ',e)
            raise ResponseError(e.code,e.msg)

    def userGenerate(self,body):
        try:
            if len(body) == 0 or 'type_user' not in body or 'id' not in body :
                print('validation request error')
                raise ResponseError(HTTP_BAD_REQUEST_CODE,HTTP_ERROR_MESSAGE)
            if len(body['type_user']) == 0 \
                    or (body['type_user'].upper() != TYPE_TEACHER and body['type_user'].upper() != TYPE_STUDENT) \
                    or body['id'] == '' :
                print('validation request empty or invalid')
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            typeUser = body['type_user']
            id = body['id']
            resultSuccess = UserServices().userGenerate(typeUser=typeUser,id=id)
            print('resultSuccess ---> ',json.dumps(resultSuccess.__dict__))
            return resultSuccess
        except Exception as e:

            print('UserController userGenerate() Exception ----> ',e)
            raise ResponseError(e.code,e.msg)