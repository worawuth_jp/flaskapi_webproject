import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from models.responses.ResponseError import ResponseError
from services.TeachersService import TeachersService
from utils.ResponseUtil import ResponseUtil


class TeachersController:
    def listTeachers(self):
        try:
            resultSuccess = TeachersService().getListAllTeacher()
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('TeachersController listTeachers() Exception ----> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)

    def listTeacherById(self,params):
        try:
            userId = params['user_id']
            resultSuccess = TeachersService().getTeacher(userId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('TeachersController listTeachers() Exception ----> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)

    def searchTeacher(self,params):
        try:
            text = params['text']
            resultSuccess = TeachersService().searchTeacher(text)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('TeachersController listTeachers() Exception ----> ',json.dumps(ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE,HTTP_ERROR_MESSAGE)

    def addTeacher(self, body):
        try:
            if 'teacher_id' not in body or 'firstname' not in body or 'lastname' not in body or 'email' not in body :
                print('TeachersController addTeacher() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['teacher_id'] == '' or body['firstname'] == '' or body['lastname'] == '' or body['email'] == '':
                print('TeachersController addTeacher() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            teacherId = body['teacher_id']
            firstname = body['firstname']
            lastname = body['lastname']
            prename = body['prename']
            email = body['email']
            resultSuccess = TeachersService().addTeacher(teacherId,prename,firstname,lastname,email)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('TeachersController addStudent() Exception ----> ',e)
            raise ResponseError(e.code, e.msg)

    def updateTeacher(self, body):
        try:
            if 'teacher_id' not in body or 'firstname' not in body or 'lastname' not in body or 'email' not in body :
                print('TeachersController updateTeacher() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['teacher_id'] == '' or body['firstname'] == '' or body['lastname'] == '' or body['email'] == '':
                print('TeachersController updateTeacher() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            teacherId = body['teacher_id']
            prename = body['prename']
            firstname = body['firstname']
            lastname = body['lastname']
            email = body['email']
            userId = body['user_id']

            resultSuccess = TeachersService().updateTeacher(userId,teacherId,prename,firstname,lastname,email)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('TeachersController updateTeacher() Exception ----> ',
                  json.dumps(e))
            raise ResponseError(e.code, e.msg)


    def deleteTeacher(self, req):
        try:
            if 'user_id' not in req :
                print('TeachersController deleteTeacher() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if req['user_id'] == '' :
                print('TeachersController updateTeacher() Exception ----> ',
                      json.dumps(ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE).__dict__))
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            userId = req['user_id']
            resultSuccess = TeachersService().deleteTeacher(userId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('TeachersController deleteTeacher() Exception ----> ',
                  json.dumps(e))
            raise ResponseError(e.code, e.msg)