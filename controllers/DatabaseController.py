import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from constants.template import SKILL_READ_TYPE,SKILL_UNDERSTAND_TYPE,SKILL_ANALYSIS_TYPE,SKILL_IMPLEMENT_TYPE
from models.responses.ResponseError import ResponseError
from services.DatabaseService import DatabaseService


class DatabaseController:
    def listDatabase(self):
        try:
            resultSuccess = DatabaseService().getAllDatabase()
            return resultSuccess
        except Exception as e:
            print('DatabaseService listDatabase() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)

    def listTable(self,req):
        try:
            db = ""
            if 'db' in req :
                db = req['db']
            resultSuccess = DatabaseService().getAllTable(db)
            return resultSuccess
        except Exception as e:
            print('DatabaseService listTable() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)

    def queryDatabase(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('sql' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['sql'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            sql = body['sql']
            resultSuccess = DatabaseService().queryDatabase(sql)
            return resultSuccess
        except Exception as e:
            print('DatabaseService queryDatabase() Exception ----> ',json.dumps(e.__dict__))
            raise ResponseError(e.code,e.msg)
