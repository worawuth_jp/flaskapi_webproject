import json

from constants.HTTP import HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE, HTTP_BAD_REQUEST_CODE
from models.responses.ResponseError import ResponseError
from services.StudentsService import StudentsService
from services.SubjectService import SubjectService
from utils.ResponseUtil import ResponseUtil


class SubjectController:
    def listSubjects(self):
        try:
            result = SubjectService().getAllSubject()
            responseSuccess = ResponseUtil().transformResponseWithData(result)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('SubjectController listSubjects() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def listSubject(self,params):
        try:
            subjectId = ""
            if 'subject_id' in params:
                subjectId = params['subject_id']
            resultSuccess = SubjectService().getSubject(subjectId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('SubjectController listSubject() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def createSubject(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('subject_code' not in body or 'subject_name' not in body or 'subject_detail' not in body or 'subject_term' not in body or 'subject_course' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['subject_code'] == '' or body['subject_name'] == '' or body['subject_detail'] == '' or body['subject_term'] == '' or body['subject_course'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            subjectCode = body['subject_code']
            subjectName = body['subject_name']
            subjectDetail = body['subject_detail']
            subjectTerm = body['subject_term']
            subjectCourse = body['subject_course']
            resultSuccess = SubjectService().createSubject(subjectCode,subjectName,subjectDetail,subjectCourse,subjectTerm)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('SubjectController createSubject() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def addGroup(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('subject_id' not in body or 'group_name' not in body or 'user_id' not in body ):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['subject_id'] == '' or body['group_name'] == '' or body['user_id'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            subjectId = body['subject_id']
            groupName = body['group_name']
            userId = body['user_id']
            resultSuccess = SubjectService().addGroup(subjectId,groupName,userId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('SubjectController addGroup() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def updateSubject(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('subject_code' not in body or 'subject_name' not in body or 'subject_detail' not in body or 'subject_term' not in body or 'subject_course' not in body or 'subject_id' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['subject_code'] == '' or body['subject_name'] == '' or body['subject_detail'] == '' or body['subject_term'] == '' or body['subject_course'] == '' or body['subject_id'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            subjectCode = body['subject_code']
            subjectName = body['subject_name']
            subjectDetail = body['subject_detail']
            subjectTerm = body['subject_term']
            subjectCourse = body['subject_course']
            subjectId = body['subject_id']
            resultSuccess = SubjectService().updateSubject(subjectId,subjectCode,subjectName,subjectDetail,subjectCourse,subjectTerm)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('SubjectController updateSubject() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def updateSubjectStatus(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ('enable' not in body or 'subject_id' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if body['enable'] == '' or body['subject_id'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            enable = body['enable']
            subjectId = body['subject_id']
            resultSuccess = SubjectService().updateSubjectStatus(subjectId,enable)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('SubjectController updateSubjectStatus() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)

    def deleteSubject(self,body):
        try:
            isBodyUndefinedOrEmpty = True if not (len(body) > 0 or body) else False
            if isBodyUndefinedOrEmpty or ( 'subject_id' not in body):
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)
            if  body['subject_id'] == '':
                raise ResponseError(HTTP_BAD_REQUEST_CODE, HTTP_ERROR_MESSAGE)

            subjectId = body['subject_id']
            resultSuccess = SubjectService().deleteSubject(subjectId)
            responseSuccess = ResponseUtil().transformResponseWithData(resultSuccess)
            print("RESPONSE ---> ", json.dumps(responseSuccess.__dict__))
            return responseSuccess
        except Exception as e:
            print('SubjectController deleteSubject() Exception ----> ',
                  json.dumps(ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE).__dict__))
            raise ResponseError(HTTP_ERROR_CODE, HTTP_ERROR_MESSAGE)
