import mysql.connector
from mysql.connector import errorcode

class DBConnector:
    def __init__(self):
        try:
            self.host = 'localhost'
            self.username = 'root'
            self.password = ''
            self.database = 'flask_api'
            self.cnx = mysql.connector.connect(user=self.username, password=self.password, host=self.host, database=self.database,
                                           charset="utf8")
            self.cursor = self.cnx.cursor(dictionary=True,buffered=True)

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        except Exception as e:
            print(e)

    def closeConnection(self):
        self.cursor.close()
        self.cnx.close()

    def getConnection(self):
        self.cnx = mysql.connector.connect(user=self.username, password=self.password, host=self.host, database=self.database,
                                           charset="utf8")
        self.cursor = self.cnx.cursor(dictionary=True,buffered=True)
        return self.cursor

    def execute(self, sql):
        result = self.cursor.execute(sql).fetchall()
        self.closeConnection()
        return result
