import datetime

from constants.HTTP import HTTP_SUCCESS_CODE, HTTP_SUCCESS_MESSAGE
from models.responses.ResponseModel import ResponseModel
from models.responses.ResponseWithData import ResponseWithData


class ResponseUtil:
    def transformResponse(self):
        try :
            code = HTTP_SUCCESS_CODE
            msg = HTTP_SUCCESS_MESSAGE
            response = ResponseModel(code,msg)
            return response

        except Exception as e:
            raise Exception(e)

    def transformResponseWithData(self,results):
        try :
            code = HTTP_SUCCESS_CODE
            msg = HTTP_SUCCESS_MESSAGE
            response = ResponseWithData(code,msg,results)
            return response

        except Exception as e:
            raise Exception(e)

    def myconverter(o):
        if isinstance(o, datetime.datetime):
            return o.__str__()
